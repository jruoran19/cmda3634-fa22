#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <util_random.h>
#include <util_test.h>
#include <util_time.h>

/* uncomment the line below to run in debugging mode (for testing code)
 * comment the line below to run in production mode (for timing code) */
#define ENABLE_DEBUG

/* set the problem sizes */
#define PROBLEM_SIZE_DEBUG 256 // = 2^8
#define PROBLEM_SIZE_TIMING 67108864 // = 2^26

/* set the bin counts */
#define BIN_COUNT_DEBUG 40
#define BIN_COUNT_TIMING 4000

/* set up the problem depending on debugging mode */
#if defined(ENABLE_DEBUG)
#define PROBLEM_SIZE PROBLEM_SIZE_DEBUG
#define BIN_COUNT BIN_COUNT_DEBUG
#else
#define PROBLEM_SIZE PROBLEM_SIZE_TIMING
#define BIN_COUNT BIN_COUNT_TIMING
#endif

/**
 * Finds the minimum value.
 *
 * Input:
 *   data       Array of samples
 *   length     Number of samples
 * 
 * Return:
 *   Minimum value of all samples
 */
float find_min(float *data, size_t length) {
  float min = data[0];
  size_t i;
  
  for (i=1; i<length; i++) {
    if (data[i] < min) {
      min = data[i];
    }
  }
  return min;
}

/**
 * Finds the maximum value.
 *
 * Input:
 *   data       Array of samples
 *   length     Number of samples
 * 
 * Return:
 *   Maximum value of all samples
 */
float find_max(float *data, size_t length) {
  float max = data[0];
  size_t i;
  
  for (i=1; i<length; i++) {
    if (max < data[i]) {
      max = data[i];
    }
  }
  return max;
}

/**
 * Sets up uniform boundaries of bins.
 *
 * Input:
 *   bin_count  Number of bins
 *   data       Array of samples
 *   length     Number of samples
 * 
 * Output:
 *   bin_ranges  Boundaries between bins (one more than `bin_count`)
 */
void setup_uniform_bin_ranges(float *bin_ranges, int bin_count, float *data, size_t length) {
  float bin_min = find_min(data, length);
  float bin_max = find_max(data, length) * 1.0001; // extend max a tiny bit, then samples are always <max
  float bin_width = bin_max - bin_min;
  int j;

#if defined(ENABLE_DEBUG)
  printf("DEBUG %s: bin_min = %g\n", __func__, bin_min);
  printf("DEBUG %s: bin_max = %g\n", __func__, bin_max);
  printf("DEBUG %s: bin_width = %g\n", __func__, bin_width);
#endif

  for (j=0; j<=bin_count; j++) {
    bin_ranges[j] = bin_width * (((float) j) / ((float) bin_count)) + bin_min;
  }
}

/**
 * Finds index of the bin in which a value falls with a linear search.
 *
 * Input:
 *   bin_ranges  Boundaries between bins (one more than `bin_count`)
 *   bin_count   Number of bins
 *   data_value  One data sample
 * 
 * Return:
 *   Index of the bin or -1 if failure.
 */
int find_bin_index_linearsearch(float *bin_ranges, int bin_count, float data_value) {
  int j;

  for (j=0; j<bin_count; j++) {
    if (bin_ranges[j] <= data_value && data_value < bin_ranges[j+1]) {
      return j;
    }
  }

  // return failure
  return -1;
}

/**
 * Finds index of the bin in which a value falls with a binary search.
 *
 * Input:
 *   bin_ranges  Boundaries between bins (one more than `bin_count`)
 *   bin_count   Number of bins
 *   data_value  One data sample
 * 
 * Return:
 *   Index of the bin or -1 if failure.
 */
int find_bin_index_binarysearch(float *bin_ranges, int bin_count, float data_value) {
  int l, r;
  l = 0;
  r = bin_count-1;
  while (l <= r) {
    int m = (l+r)/2;
    if (bin_ranges[m] <= data_value && data_value < bin_ranges[m+1]) {
      return m;
    } else if (bin_ranges[m] > data_value) {
      r = m - 1;
    } else {
      l = m + 1;
    }
  }
  // return failure
  return -1;
}

/**
 * Counts how many samples belong into each bin. Uses linear search to find bin.
 *
 * Input:
 *   bin_ranges  Boundaries between bins (one more than `bin_count`)
 *   bin_count   Number of bins
 *   data        Array of samples
 *   length      Number of samples
 * 
 * Output:
 *   bins  Counts of samples per bin (size `bin_count`)
 */
void fill_bins_linearsearch(int *bins, float *bin_ranges, int bin_count, float *data, size_t length) {
  size_t i;
  int j;

  /* initialize empty bins  */
  for (j=0; j<bin_count; j++) {
    bins[j] = 0;
  }
  
  /* count samples */
  for (i=0; i<length; i++) {
    /* search for bin index */
    // find bin index with `find_bin_index_linearsearch`
    j = find_bin_index_linearsearch(bin_ranges, bin_count, data[i]);

    /* increment corresponding bin counter */
    // increment bin counter if index `j` indicates that search did not fail;
    // print an error message if index search did fail
    if (j != -1) {
      bins[j] += 1;
    } else {
      printf("one data sample is out of bin ranges");
    }
  }
}

/**
 * Counts how many samples belong into each bin. Uses binary search to find bin.
 *
 * Input:
 *   bin_ranges  Boundaries between bins (one more than `bin_count`)
 *   bin_count   Number of bins
 *   data        Array of samples
 *   length      Number of samples
 * 
 * Output:
 *   bins  Counts of samples per bin (size `bin_count`)
 */
void fill_bins_binarysearch(int *bins, float *bin_ranges, int bin_count, float *data, size_t length) {
  size_t i;
  int j;

  /* initialize empty bins  */
  for (j=0; j<bin_count; j++) {
    bins[j] = 0;
  }
  
  /* count samples */
  for (i=0; i<length; i++) {
    /* search for bin index */
    // find bin index with `find_bin_index_binarysearch`
    j = find_bin_index_binarysearch(bin_ranges, bin_count, data[i]);

    /* increment corresponding bin counter */
    // increment bin counter if index `j` indicates that search did not fail;
    // print an error message if index search did fail
    if (j != -1) {
      bins[j] += 1;
    } else {
      printf("one data sample is out of bin ranges");
    }
  }
}

/**
 * Prints the number of samples in each bin. And, at the end, prints the total
 * sum of samples in all of the bins.
 *
 * Input:
 *   bins         Counts of samples per bin (size `bin_count`)
 *   bin_ranges   Boundaries between bins (one more than `bin_count`)
 *   bin_count    Number of bins
 */
void print_bins(int *bins, float *bin_ranges, int bin_count) {
  long unsigned int sum = 0;
  int j;
  
  printf("========================================\n");
  for (j=0; j<bin_count; j++) {
    printf("%4d | [%+08.4f,%+08.4f) | #samples = %d\n", j, bin_ranges[j], bin_ranges[j+1], bins[j]);
    sum += (unsigned long long) bins[j];
  }
  printf("----------------------------------------\n");
  printf("Total number of samples in all bins: %lu\n", sum);
  printf("========================================\n");
}

/**
 * MAIN
 */
int main(int argc, char **argv) {
  const size_t N = PROBLEM_SIZE;
  const float given_mean = 0.0;
  const float given_std = 0.7;
  const int bin_count = BIN_COUNT;
  float *data;
  float *bin_ranges;
  int *bins;

  /* set random seed for reproducability */
  srand(3634);

  /* print the problem settings */
  printf("Problem size N = %lu\n", (long unsigned int) N);
  printf("Number of bins = %d\n", bin_count);
  printf("Given mean = %g\n", given_mean);
  printf("Given std  = %g\n", given_std);

  /* allocate `data` as a float array of size `N` */
  data = (float*) malloc(N*sizeof(float));
  /* allocate `bin_ranges` as a float array of size `bin_count + 1` */
  bin_ranges = (float*) malloc((bin_count + 1)*sizeof(float));
  /* allocate `bins` as an int array of size `bin_count` */
  bins = (int*) malloc(bin_count*sizeof(int));
  
  /* set up `data` */
  generate_random_normal_data(data, N, given_mean, given_std);
  transform_normal_to_lognormal(data, N);
  
  /* set up uniform `bin_ranges` based on samples in `data` */
  setup_uniform_bin_ranges(bin_ranges, bin_count, data, N);
  
  printf("========================================\n");

#if defined(ENABLE_DEBUG)
  printf("  Test functions\n");
  printf("----------------------------------------\n");

  test_find_bins(bin_ranges, bin_count, data, N, fill_bins_linearsearch, fill_bins_binarysearch);
#else
  printf("  Time function calls\n");
  printf("----------------------------------------\n");

  printf("- fill_bins_linearsearch\n");
  time_fill_bins(bins, bin_ranges, bin_count, data, N, fill_bins_linearsearch);
  printf("- fill_bins_binarysearch\n");
  time_fill_bins(bins, bin_ranges, bin_count, data, N, fill_bins_binarysearch);
#endif

  /* deallocate */
  free(data);
  free(bin_ranges);
  free(bins);

  printf("========================================\n");

  return 0;
}
