#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <util_random.h>
#include <util_test.h>
#include <util_time.h>

/* uncomment the line below to run in debugging mode (for testing code)
 * comment the line below to run in production mode (for timing code) */
// #define ENABLE_DEBUG

/* set the problem sizes */
#define PROBLEM_SIZE_DEBUG 1048576 // = 2^20
#define PROBLEM_SIZE_TIMING 1073741824 // = 2^30

/* set up the problem depending on debugging mode */
#if defined(ENABLE_DEBUG)
#define PROBLEM_SIZE PROBLEM_SIZE_DEBUG
#else
#define PROBLEM_SIZE PROBLEM_SIZE_TIMING
#endif

/**
 * Approximates the value Pi with a Monte Carlo integration algorithm.  The
 * procedure is to generate random samples from a uniform distribution
 * (interval = [0,1]^2), and accept or reject a sample depending whether it is
 * inside or outsize one quarter of the unit circle, which is centered at
 * (x,y)=(0,0).
 *
 * [1] https://en.wikipedia.org/wiki/Monte_Carlo_integration
 *
 * Input:
 *   length   Number of generated random samples
 *
 * Return:
 *   Approximation of the value of Pi
 */
double approximate_pi(size_t n_samples) {
  size_t n_accepted_samples;
  size_t i;

  /* generate samples  */
  n_accepted_samples = 0;
  for (i=0; i<n_samples; ++i) {
    /* generate random uniform samples */
    float x = random_uniform();
    float y = random_uniform();

    /* count accepted sample if it is inside the unit circle */
    if ((x*x+y*y) <= 1.0) {
      n_accepted_samples++;
    }
  }

  /* return ratio of accepted vs. total number of samples;
   * multiply by four to get the area of the complete unit circle */
  return 4.0 * ((double) n_accepted_samples) / ((double) n_samples);
}

/**
 * OpenMP implementation of the approximation of Pi.
 * Uses multiple variables for storing intermediate results of threads.
 *
 * Input:
 *   length   Number of generated random samples
 *
 * Return:
 *   Approximation of the value of Pi
 */
double approximate_pi_omp_multistorage(size_t n_samples) {
  // Implement an OpenMP version of the approximation of Pi.
  // Use multiple variables for storing intermediate results of threads.
  int ompsize;

  #pragma omp parallel shared(ompsize)
  {
    ompsize = omp_get_num_threads();
  }

  size_t n_accepted_samples;
  size_t flags[ompsize];
  size_t i;

  /* generate samples  */
  n_accepted_samples = 0;

#pragma omp parallel shared(flags, ompsize, n_samples) private(i)
  {
    i = omp_get_thread_num();
    size_t chunk = n_samples/(size_t)ompsize;
    if (i == ompsize-1) {
      chunk += n_samples % (size_t)ompsize;
    }
    size_t accepted = 0;
    for (size_t j = 0; j < chunk; j++)
    {
      /* generate random uniform samples */
      float x = random_uniform();
      float y = random_uniform();
      /* count accepted sample if it is inside the unit circle */
      if ((x*x+y*y) <= 1.0) {
        accepted++;
      }
    }
    flags[i]=accepted;
  }
    
  n_accepted_samples = 0;
  for (i = 0; i < ompsize; i++) {
    n_accepted_samples += flags[i];
  }

  /* return ratio of accepted vs. total number of samples;
   * multiply by four to get the area of the complete unit circle */
  return 4.0 * ((double) n_accepted_samples) / ((double) n_samples);
}

/**
 * OpenMP implementation of the approximation of Pi.
 * Uses the reduction clause of OpenMP to sum up contributions from all threads.
 *
 * Input:
 *   length   Number of generated random samples
 *
 * Return:
 *   Approximation of the value of Pi
 */
double approximate_pi_omp_reduction(size_t n_samples) {
  // Implement an OpenMP version of the approximation of Pi.
  // Use the reduction clause of OpenMP to sum up contributions from all threads.
  int ompsize;

  #pragma omp parallel shared(ompsize)
  {
    ompsize = omp_get_num_threads();
  }

  size_t n_accepted_samples;
  size_t i;

  /* generate samples  */
  n_accepted_samples = 0;

#pragma omp parallel shared(ompsize, n_samples) private(i) reduction(+:n_accepted_samples)
  {
    i = omp_get_thread_num();
    size_t chunk = n_samples/(size_t)ompsize;
    if (i == ompsize-1) {
      chunk += n_samples % ompsize;
    }
    size_t accepted = 0;
    for (size_t j = 0; j < chunk; j++)
    {
      /* generate random uniform samples */
      float x = random_uniform();
      float y = random_uniform();
      /* count accepted sample if it is inside the unit circle */
      if ((x*x+y*y) <= 1.0) {
        accepted++;
      }
    }
    n_accepted_samples+=accepted;
  }

  /* return ratio of accepted vs. total number of samples;
   * multiply by four to get the area of the complete unit circle */
  return 4.0 * ((double) n_accepted_samples) / ((double) n_samples);
}

int main(int argc, char **argv) {
  const size_t N_SAMPLES = PROBLEM_SIZE;
  int ompsize;

  #pragma omp parallel shared(ompsize)
  { // FORK: begin parallel
    ompsize = omp_get_num_threads();
  } // JOIN: end parallel
  printf("Parallel environment: OpenMP size %i\n", ompsize);

  /* fix a random seed for reproducability */
  srand(3634);

  /* print the problem settings */
  printf("Problem size N = %lu\n", (long unsigned int) N_SAMPLES);

  printf("========================================\n");
  fflush(stdout); // flush buffered print output

#if defined(ENABLE_DEBUG)
  printf("  Test function(s)s\n");
  printf("----------------------------------------\n");

  printf("- approximate_pi\n");
  test_pi_approx(N_SAMPLES, approximate_pi);

  printf("- approximate_pi_omp_multistorage\n");
  test_pi_approx(N_SAMPLES, approximate_pi_omp_reduction);

  printf("- approximate_pi_omp_reduction\n");
  test_pi_approx(N_SAMPLES, approximate_pi_omp_reduction);
#else
  printf("  Time function(s)s\n");
  printf("----------------------------------------\n");

  printf("- approximate_pi\n");
  time_pi_approx(N_SAMPLES, approximate_pi);

  printf("- approximate_pi_omp_multistorage\n");
  time_pi_approx(N_SAMPLES, approximate_pi_omp_multistorage);

  printf("- approximate_pi_omp_reduction\n");
  time_pi_approx(N_SAMPLES, approximate_pi_omp_reduction);
#endif

  printf("========================================\n");

  return 0;
}
