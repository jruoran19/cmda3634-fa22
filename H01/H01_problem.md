# Assigment 01: Collatz iteration


## Honor Code:
By submitting this assignment, you acknowledge that you have adhered to the
Virginia Tech Honor Code and attest to the following:
> I have neither given nor received unauthorized assistance on this assignment.
> The work I am presenting is ultimately my own.


## Submission
- The submission of this homework is under your own and private Git project at:
  <https://code.vt.edu>
- You do not need to submit anything to Canvas.


## Background
The Collatz sequence of a positive integer $n$ is $a_0$, $a_1$, ... , $a_N$ where
- $a_0 = n$
- If $a_i$ is even, then $a_{i+1} = a_i/2$
- If $a_i$ is odd, then $a_{i+1} = 1 + 3 a_i$
- $N$ is the smallest index such that $a_N = 1$


## Prepare
- Visit <https://code.vt.edu/jrudi/cmda3634-fa22> and download the notebook
  `H01.ipynb` from the folder `H01`.
- Visit <https://colab.research.google.com/> and open the downloaded notebook
  as follows: select the "Upload" tab, then choose the downloaded file
  `H01.ipynb`.
  (Alternatively, upload `H01.ipynb` to your Google drive and double-click on it.)


## Q1: Design an algorithm
Think about how you would design the algorithm: 

> A loop that prints the Collatz sequence with comma separators.

Write the algorithm down in form of a pseudo-code that is similar to the
pseudo-code presented in the lecture.  The pseudo-code needs to capture what
you consider to be the key steps in your algorithm.


## Q2: Write code

### Warning
> This is a well known problem, and it isn't hard to find code for it online.
> Don't just copy code from the Internet. Your solution should be yours.
> The attempt to google for a solution is considered a violation of VT's honor
> code.

Complete the part that is indicated in the notebook cell that contains a
template of the C code.

Implement in the C language:

> A loop that prints the Collatz sequence with comma separators.

The input number `int n` is already created in the template notebook
`H01.ipynb`.  If the input `n` is negative, your program should print a helpful
message and exit.

To give an example what a sequence should look like, if `n = 11`, then your
program should produce the output:
```
11,34,17,52,26,13,40,20,10,5,16,8,4,2,1
```

**Note**: Instructors may test your program with other inputs as well.

When finished, save and download the notebook with the solution to your computer.


## Q3: Upload solution
Open your personal Git project in the web browser:
- Visit <https://code.vt.edu/>
- Select the project called `cmda3634` (this project was created in FSA 1)

Navigate to the folder of this assignment:
- Click on `H01`

Upload the notebook file that contains your solution:
- In the top-center of the browser, find where it's written `cmda3634/H01/`
- Click on the `+` sign next to it, and then select "Upload file"
- In the new window that popped up, drop or select the file `H01.ipynb` **that contains your solution**. Click the "Upload file" button.
- View the uploaded file to verify one more time that it contains your solution.
