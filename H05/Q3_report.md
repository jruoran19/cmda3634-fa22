# Results

## optimization: -O0

|Function name|Wall-clock time [s]|
|:---|---:|
|fsum_sequential                |   1.872e-03   |
|fsum_sequential_unroll_var     |   2.018e-03   |
|fsum_sequential_unroll_block   |   5.838e-04   |
|fsum_subdivide                 |   1.441e-03   |
|dsum_sequential                |   1.917e-03   |
|dsum_sequential_unroll_var     |   1.916e-03   |
|dsum_sequential_unroll_block   |   5.670e-04   |
|dsum_subdivide                 |   1.498e-03   |

## optimization: -O3

|Function name|Wall-clock time [s]|
|:---|---:|
|fsum_sequential                |   4.672e-04   |
|fsum_sequential_unroll_var     |   5.016e-04   |
|fsum_sequential_unroll_block   |   2.155e-04   |
|fsum_subdivide                 |   1.802e-04   |
|dsum_sequential                |   5.023e-04   |
|dsum_sequential_unroll_var     |   4.918e-04   |
|dsum_sequential_unroll_block   |   2.732e-04   |
|dsum_subdivide                 |   3.692e-04   |

## optimization: -Ofast -march=znver2 -mavx2

|Function name|Wall-clock time [s]|
|:---|---:|
|fsum_sequential                |   1.000e-04   |
|fsum_sequential_unroll_var     |   2.802e-04   |
|fsum_sequential_unroll_block   |   9.662e-05   |
|fsum_subdivide                 |   2.169e-04   |
|dsum_sequential                |   2.057e-04   |
|dsum_sequential_unroll_var     |   3.116e-04   |
|dsum_sequential_unroll_block   |   2.164e-04   |
|dsum_subdivide                 |   3.951e-04   |

## observations

- The `*sequential_unroll_var` version is almost always the slowest of all the optimization options.

- The `*sequential_unroll_block` version is almost always the fastest of all the optimization options.

- The `-O3` version is faster than the `-O0` version.

- The `-Ofast` version is fastest on any sequential functions.