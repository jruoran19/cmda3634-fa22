#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* set the array length; it has to be a power of 2 and greater 8 */
#define PROBLEM_SIZE 1048576

/* set the number of repeats when timing runtime */
#define TIMING_REPEAT 1024

/**
 * Computes the sum of a float array `x` of size `n`.
 * Returns the value of the sum.
 *
 * This is the reference implementation. Do not change it!
 */
float fsum_sequential(float *x, int n) {
  float sum = 0.0;
  int i;

  for (i=0; i<n; i++) {
    sum += x[i];
  }
  return sum;
}

float fsum_sequential_unroll_var(float *x, int n) {
  float sum = 0;
  for (int i = 0; i < n; i+=8) {
    sum += x[i+0];
    sum += x[i+1];
    sum += x[i+2];
    sum += x[i+3];
    sum += x[i+4];
    sum += x[i+5];
    sum += x[i+6];
    sum += x[i+7];
  }
  return sum;
}

float fsum_sequential_unroll_block(float *x, int n) {
  float sum[8] = {0};
  for (int i = 0; i < n; i+=8) {
    sum[0] += x[i+0];
    sum[1] += x[i+1];
    sum[2] += x[i+2];
    sum[3] += x[i+3];
    sum[4] += x[i+4];
    sum[5] += x[i+5];
    sum[6] += x[i+6];
    sum[7] += x[i+7];
  }
  return sum[0]+sum[1]+sum[2]+sum[3]+sum[4]+sum[5]+sum[6]+sum[7];
}

float fsum_subdivide(float *x, int n) {
  if (n == 1) {
    return x[0];
  }
  for (int i = 0; i < n/2; i++) {
    x[i]+=x[n/2+i];
  }
  return fsum_subdivide(x, n/2);
}

/**
 * Computes the sum of a double array `x` of size `n`.
 * Returns the value of the sum.
 *
 * This is the reference implementation. Do not change it!
 */
double dsum_sequential(double *x, int n) {
  double sum = 0.0;
  int i;

  for (i=0; i<n; i++) {
    sum += x[i];
  }
  return sum;
}

double dsum_sequential_unroll_var(double *x, int n) {
  double sum = 0;
  for (int i = 0; i < n; i+=4) {
    sum += x[i+0];
    sum += x[i+1];
    sum += x[i+2];
    sum += x[i+3];
  }
  return sum;
}

double dsum_sequential_unroll_block(double *x, int n) {
  double sum[4] = {0};
  for (int i = 0; i < n; i+=4) {
    sum[0] += x[i+0];
    sum[1] += x[i+1];
    sum[2] += x[i+2];
    sum[3] += x[i+3];
  }
  return sum[0]+sum[1]+sum[2]+sum[3];
}

double dsum_subdivide(double *x, int n) {
  if (n == 1) {
    return x[0];
  }
  for (int i = 0; i < n/2; i++) {
    x[i]+=x[n/2+i];
  }
  return dsum_subdivide(x, n/2);
}

/* Provided for you: Tests accuracy of a sum function. */
void test_accuracy_fsum(float *x, int n, float(*fn)(float*, int));
void test_accuracy_dsum(double *x, int n, double(*fn)(double*, int));

/* Provided for you: Times the duration of an sum function. */
void time_fsum(float *x, int n, float(*fn)(float*, int));
void time_dsum(double *x, int n, double(*fn)(double*, int));

/**
 * Define the main function.
 */
int main(int argc, char **argv) {
  const int N = PROBLEM_SIZE;
  float *fx;
  double *dx;

  /* set random seed for reproducability */
  srand(3634);

  printf("========================================\n");
  printf("  Test fsum\n");
  printf("----------------------------------------\n");

  /* allocate */
  fx = (float*) malloc(N*sizeof(float));

  /* test accuracy of fsum functions */

  printf("- fsum_sequential_unroll_var\n");
  /* run `test_accuracy_fsum` with `fsum_sequential_unroll_var` */
  test_accuracy_fsum(fx, N, fsum_sequential_unroll_var);

  printf("- fsum_sequential_unroll_block\n");
  /* run `test_accuracy_fsum` with `fsum_sequential_unroll_block` */
  test_accuracy_fsum(fx, N, fsum_sequential_unroll_block);

  printf("- fsum_subdivide\n");
  /* run `test_accuracy_fsum` with `fsum_subdivide` */
  test_accuracy_fsum(fx, N, fsum_subdivide);

  printf("----------------------------------------\n");

  /* time fsum functions */

  printf("- fsum_sequential\n");
  time_fsum(fx, N, fsum_sequential); // Note that the 3rd argument is a function

  printf("- fsum_sequential_unroll_var\n");
  /* run `time_fsum` with `fsum_sequential_unroll_var` */
  time_fsum(fx, N, fsum_sequential_unroll_var);

  printf("- fsum_sequential_unroll_block\n");
  /* run `time_fsum` with `fsum_sequential_unroll_block` */
  time_fsum(fx, N, fsum_sequential_unroll_block);

  printf("- fsum_subdivide\n");
  /* run `time_fsum` with `fsum_subdivide` */
  time_fsum(fx, N, fsum_subdivide);

  /* deallocate */
  free(fx);

  printf("========================================\n");
  printf("  Test dsum\n");
  printf("----------------------------------------\n");

  /* allocate */
  dx = (double*) malloc(N*sizeof(double));

  /* test accuracy of dsum functions */

  printf("- dsum_sequential_unroll_var\n");
  /* run `test_accuracy_dsum` with `dsum_sequential_unroll_var` */
  test_accuracy_dsum(dx, N, dsum_sequential_unroll_var);

  printf("- dsum_sequential_unroll_block\n");
  /* run `test_accuracy_dsum` with `dsum_sequential_unroll_block` */
  test_accuracy_dsum(dx, N, dsum_sequential_unroll_block);

  printf("- dsum_subdivide\n");
  /* run `test_accuracy_dsum` with `dsum_subdivide` */
  test_accuracy_dsum(dx, N, dsum_subdivide);

  printf("----------------------------------------\n");

  /* time dsum functions */

  printf("- dsum_sequential\n");
  time_dsum(dx, N, dsum_sequential); // Note that the 3rd argument is a function

  printf("- dsum_sequential_unroll_var\n");
  /* run `time_dsum` with `dsum_sequential_unroll_var` */
  time_dsum(dx, N, dsum_sequential_unroll_var);

  printf("- dsum_sequential_unroll_block\n");
  /* run `time_dsum` with `dsum_sequential_unroll_block` */
  time_dsum(dx, N, dsum_sequential_unroll_block);

  printf("- dsum_subdivide\n");
  /* run `time_dsum` with `dsum_subdivide` */
  time_dsum(dx, N, dsum_subdivide);

  /* deallocate */
  free(dx);

  printf("========================================\n");

  return 0;
}

/* DO NOT CHANGE THE CODE BELOW */

void set_random_uniform_f(float *x, int n) {
  int i;

  for (i=0; i<n; i++) {
    x[i] = (float)rand() / (float)RAND_MAX;
  }
}

void set_random_uniform_d(double *x, int n) {
  int i;

  for (i=0; i<n; i++) {
    x[i] = (double)rand() / (double)RAND_MAX;
  }
}

void test_accuracy_fsum(float *x, int n, float(*fn)(float*, int)) {
  float sum_ref, sum_out;

  set_random_uniform_f(x, n);
  sum_ref = fsum_sequential(x, n);
  sum_out = fn(x, n);
  printf("%s: sum = %.8e, ref = %.8e ; error abs = %g, rel = %.6e\n", __func__,
         sum_out, sum_ref,
         fabs(sum_out - sum_ref), fabs(sum_out - sum_ref)/fabs(sum_ref));
}

void test_accuracy_dsum(double *x, int n, double(*fn)(double*, int)) {
  double sum_ref, sum_out;

  set_random_uniform_d(x, n);
  sum_ref = dsum_sequential(x, n);
  sum_out = fn(x, n);
  printf("%s: sum = %.16e, ref = %.16e ; error abs = %g, rel = %.6e\n", __func__,
         sum_out, sum_ref,
         fabs(sum_out - sum_ref), fabs(sum_out - sum_ref)/fabs(sum_ref));
}

void time_fsum(float *x, int n, float(*fn)(float*, int)) {
  int k;
  clock_t tic, toc;
  double elapsed = 0.0;

  for (k=0; k<TIMING_REPEAT; k++) {
    set_random_uniform_f(x, n);
    /* call function and time */
    tic = clock();
    fn(x, n);
    toc = clock();
    /* calculate elapsed time */
    elapsed += (toc-tic)/(double)CLOCKS_PER_SEC;
  }

  printf("%s: elapsed time ~ %.3f s, time per call ~ %.3e s\n", __func__,
         elapsed, elapsed/TIMING_REPEAT);
}

void time_dsum(double *x, int n, double(*fn)(double*, int)) {
  int k;
  clock_t tic, toc;
  double elapsed = 0.0;

  for (k=0; k<TIMING_REPEAT; k++) {
    set_random_uniform_d(x, n);
    /* call function and time */
    tic = clock();
    fn(x, n);
    toc = clock();
    /* calculate elapsed time */
    elapsed += (toc-tic)/(double)CLOCKS_PER_SEC;
  }

  printf("%s: elapsed time ~ %.3f s, time per call ~ %.3e s\n", __func__,
         elapsed, elapsed/TIMING_REPEAT);
}
