/*

BACKGROUND

A loop "runs efficiently in parallel" if its execution time with P threads
should be expected to be about 1/P times as long as with one thread while
leaving the effect/result of the loop unchanged.

INSTRUCTIONS

Review the provided file `parllelize_or_not.c`. Understand what goes on in the
for-loop and in any functions that are called in the loop body. Compile and run
the code (in debugging mode) to show the output and understand how this output
was created with the code.

For each for-loop, the goal is to understand two things:
  (1) Can the iterations of the loop run in parallel? -> **feasibility**
  (2) Is the loop expected to run faster in parallel with more and more
      threads? -> **efficiency**

The task for each for-loop is to decide which of the following statements applies.

First, make a judgement about the **feasibility** of parallelization:
  (a) The loop is able to run in parallel. It simply requires the
      OpenMP directive `#pragma omp parallel for`.
  (b) The loop's code requires some modification or a more sophisticated pragma
      directive is needed before it can execute correctly in parallel.
      - In this case, explain why statement (a) is not true.
      - If the loop body requires significant changes for parallelization,
        describe the changes. The loop body modifications may require some
        cleverness or alternated arithmetics.
  (c) The loop is impossible to parallelize.
      - In this case, explain what is preventing the parallelization of the loop.

Second, and if the first judgement was (a) or (b), make a judgement about the
**efficiency** of parallelization:
  (a) The loop will run **efficiently** in parallel. It simply requires the
      OpenMP directive `#pragma omp parallel for`.
  (b) The loop is expected to have difficulties scaling efficiently. Some
      modifications are needed to avoid "bottlenecks for scalability" (e.g.,
      improve load balancing)
      - In this case, explain why statement (a) is not true.
      - Describe the changes to the loop or additional OpenMP directives to
        improve the efficiency.
  (c) The loop will not benefit from parallelization.
      - In this case, explain why the loop is a poor candidate for parallelization.

*/

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define PROBLEM_SIZE 20

/* declare functions that will be defined later */
int totient(int);
int alternate_signs();
int collatzIter(int);
int collatzNIter(int);
void display(const char*, int*, int);

/**
 * Main function
 */
int main(int argc, char **argv) {
  const int N = PROBLEM_SIZE;
  int n;
  int *v = (int*) malloc(N*sizeof(int));

  /* Loop 1
   * Set v[n] to the totient function of n */
  /*
  Answer, feasibility:
  The loop can be parallelized, because each iteration only depends on the number n

  Answer, efficiency:
  The loop has the different amout of work per iteration,
  because each iteration are independent
  */
 #pragma omp parallel for
  for (n=0; n<N; n++) {
    v[n] = totient(n);
  }
  display("Loop 1", v, N);
  

  /* Loop 2
   * Swap consecutive pairs of entries of v */
  /*
  Answer, feasibility:
  The loop can be parallelized, because each iteration only depends on the number n

  Answer, efficiency:
  The loop has the different amout of work per iteration,
  because each iteration are independent
  */
 #pragma omp parallel for
  for (n=0; n<N; n+=2) {
    int temp = v[n+1];
    v[n+1] = v[n];
    v[n] = temp;
  }
  display("Loop 2", v, N);

  /* Loop 3
   * Set v[n] to {1, -1, 1, ... } */
  /*
  Answer, feasibility:
  The loop can be parallelized,
  but we need to change the alternate_signs function based on the iteration number n

  Answer, efficiency:
  The loop has the different amout of work per iteration,
  because each iteration are independent
  */
 #pragma omp parallel for
  for (n=0; n<N; n++) {
    v[n] = n % 2 == 0?1:-1;
  }
  display("Loop 3", v, N);

  /* Loop 4
   * Set v[n] to the nth Fibonacci number */
  /*
  Answer, feasibility:
  The loop cannot be parallelized, because each iteration depends on previous one

  Answer, efficiency:
  The loop has the same amout of work per iteration,
  since cannot be parallelized
  */
  // v[0] = 0;
  // v[1] = 1;
  // for (n=2; n<N; n++) {
  //   v[n] = v[n-1] + v[n-2];
  // }
  // display("Loop 4", v, N);

  /* Loop 5
   * Set v[n] to 1 if n is a "lucky" number, 0 otherwise */
  /*
  Answer, feasibility:
  The loop cannot be parallelized, because each iteration depends on previous one

  Answer, efficiency:
  The loop has the same amout of work per iteration,
  since cannot be parallelized.
  */
  // v[0] = 0;
  // v[1] = 1;
  // for (n=2; n<N; n++) {
  //   if (v[n] == 0) {
  //     continue; // skip to next loop iteration
  //   }
  //   temp = 0;
  //   for (i=0; i<N; i++) {
  //     if (v[i] != 0) {
  //       temp++;
  //       if (temp == n) {
  //         v[i] = 0;
  //         temp = 0;
  //       }
  //       else {
  //         v[i] = 1;
  //       }
  //     }
  //   }
  // }
  // display("Loop 5", v, N);

  /* Loop 6
   * Make v[n] the second term in n's Collatz sequence */
  /*
  Answer, feasibility:
  The loop can be parallelized, because each iteration only depends on the number n

  Answer, efficiency:
  The loop has the different amout of work per iteration,
  because each iteration are independent
  */
 #pragma omp parallel for
  for (n=1; n<N; n++) {
    v[n] = collatzIter(n);
  }
  display("Loop 6", v, N);

  /* Loop 7
   * Make v[n] the nth term of the Collatz sequence starting with 137 */
  /*
  Answer, feasibility:
  The loop can be parallelized, because each iteration only depends on the number n

  Answer, efficiency:
  The loop is expected to have difficulties scaling efficiently,
  since the inner loop of collatzNIter function will run different times,
  we can first calculate the value of foo for each n
  */
  v[0] = 137;
  int foo[N];
  foo[0] = 137;
  for (int i = 1; i < N; i++) {
    foo[i] = collatzIter(foo[i-1]);
  }
  #pragma omp parallel for
  for (n=1; n<N; n++) {
    v[n] = foo[n];
  }
  display("Loop 7", v, N);

  /* Loop 8
   * Make v[n] the Collatz convergence time of n */
  /*
  Answer, feasibility:
  The loop can be parallelized, because each iteration only depends on the number n

  Answer, efficiency:
  The loop has the different amout of work per iteration,
  because each iteration are independent
  */
 #pragma omp parallel for
  for (n=0; n<N; n++) {
    v[n] = collatzNIter(n);
  }
  display("Loop 8", v, N);

  /* Loop 9
   * Cyclically shift the entries of v one down */
  /*
  Answer, feasibility:
  The loop cannot be parallelized, because each iteration depends on previous one

  Answer, efficiency:
  The loop has the same amout of work per iteration,
  since cannot be parallelized
  */
  // temp = v[0];
  // for (n=0; n<N-1; n++) {
  //   v[n] = v[n+1];
  // }
  // v[N-1] = temp;
  // display("Loop 9", v, N);

  free(v);
  return 0;
}

/*
  DO NOT MODIFY THE FUNCTIONS BELOW
  You should think of them as library functions that are outside of your
  control.
*/

int gcd(int a, int b) {
  if (b == 0) {
    return a;
  }
  return gcd(b, a%b);
}

/**
 * Computes Euler's totient function.
 */
int totient(int n) {
  int tot = 1;
  for (int i=2; i<n; i++) {
    if (gcd(n,i) == 1) {
      tot++;
    }
  }
  return tot;
}

/**
 * Switches signes back and forth. Note the use of the `static` specifyier.
 *
 * Definition of `static` storage duration: The storage duration is the entire
 * execution of the program, and the value stored in the object is initialized
 * only once, prior to main function.
 * If we declare a static variable inside of a function, the compiler will
 * generate a global variable with a (hidden) unique name. The difference
 * between static function variables and global variables is in scope of
 * visibility only.
 */
int alternate_signs() {
  static int sign = -1;
  sign *= -1;
  return sign;
}

/**
 * Performs one Collatz iteration.
 */
int collatzIter(int n) {
  if (n%2 == 0) {
    return n/2;
  }
  return 3*n + 1;
}

/**
 * Counts the number of iterations to complete Collatz sequence.
 */
int collatzNIter(int n) {
  int counter = 0;
  while (n > 1) {
    n = collatzIter(n);
    counter++;
  }
  return counter;
}

/**
 * Prints an array.
 */
void display(const char *title, int *v, int N) {
  printf("========================================\n");
  printf("  %s\n", title);
  printf("----------------------------------------\n");
  for(int n=0; n<N; n++) {
    printf("v[%2d] = %d\n", n, v[n]);
  }
  printf("\n");
}

