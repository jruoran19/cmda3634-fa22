# H09: Help, hints, bugfixes

- Setting begin and end indices as covered in lecture 18:
  ```c
  // int N ... is the problem size
  #pragma omp parallel
  {
    int ompsize   = omp_get_num_threads();
    int ompthread = omp_get_thread_num();
    int index_begin = (N*ompthread)/ompsize;
    int index_end   = (N*(ompthread + 1))/ompsize;
    for (int i=index_begin; i<index_end; i++) {
      // compute something
    }
  }
  ```

- **Loop 3**:
  There could be some ambiguity what's allowed to parallelize the loop, because
  the code of the function `alternate_signs()` is what needs to be modified,
  and, at the same time, there is a comment `DO NOT MODIFY THE FUNCTIONS
  BELOW`.
  To clarify, it is allowed to take the content of the function
  `alternate_signs()` and bring it inside the `for` loop.  Then the
  modifications for the parallelizations can be done without altering the
  functions at the bottom of the file.

- **Loop 3**: This loop is a bit tricky when multiple threads are involved,
  because what would happen if we assume that `sign = -1` for every thread,
  and, at the same time, what if a thread's first index is odd? That means,
  what if `n%2==1`? Does this thread still set the right value for `v[n]`?

- **Loop 9**:
  Recall how we manually set `index_begin` and `index_end` for each thread in
  lecture 18.  Knowing the begin and end indices, you can tackle the race
  condition that happens for setting `v[index_end-1]` for each thread.

- There are multiple solutions to parallelize  **Loop 9**.
  In one possible solution, it may be useful to use the pragma:
  ```
  #pragma omp barrier
  ```
  which makes each thread wait until all of the threads have reached the line
  of the barrier pragma, and only when all threads have arrived there, the
  threads can continue.

- In **Q2**, a single job script, called `submission_parallel.sh`, will produce
  multiple output files:
  - `slurm-XYZ-ompsize001.out`
  - `slurm-XYZ-ompsize002.out`
  - `slurm-XYZ-ompsize005.out`
  - `slurm-XYZ-ompsize010.out`
  These files are outputs of your program with different numbers of threads,
  namely 1, 2, 5, and 10.  You need to check each of the files that the printed
  results are the same as in the output of the sequential run.

  A single job script is doing multiple runs with different numbers of threads,
  and I've written it that way to make your life easier.

- To quickly compare two `.out` files, there is a neat command line tool.
  Assuming that `ls` shows you these two files (among other files):
  ```
  slurm-RST-sequential.out
  slurm-XYZ-ompsize002.out
  ```
  Then you can run this command:
  ```
  vimdiff slurm-RST-sequential.out slurm-XYZ-ompsize002.out
  ```
  to see the differences of these two files side-by-side. (If nothing is
  different, the display will look empty).  To exit `vimdiff`, type the command
  `:q` twice!
