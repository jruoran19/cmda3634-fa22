#!/bin/bash

# Help: Run this script as: source setup_env.sh

# set modules to system's default
module reset

# load compilers
#module load gompi
module load foss/2020a
#module load intel/2019b
