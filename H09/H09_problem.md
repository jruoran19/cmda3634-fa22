# Assigment 09 (40 points)


## Honor Code:
By submitting this assignment, you acknowledge that you have adhered to the
Virginia Tech Honor Code and attest to the following:
> I have neither given nor received unauthorized assistance on this assignment.
> The work I am presenting is ultimately my own.

### Using Google search
It is allowed to use Google to search for the meaning of compiler errors &
warnings, explanation for Git messages, or any sort of message that you are
being shown in the terminal.  Using Google skillfully to improve your
understanding of a subject is an important skill in programming.


## Submission
- The online submission of this homework is under your own and private Git
  project at: <https://code.vt.edu>
- The offline submission of this homework is in class or at the office 474
  McBryde. Slide the hard-copy under the door if it's closed (Note that the
  McBryde building has automatically locking doors that lock sometime in the
  evening.)
- You do not need to submit anything to Canvas.

### Submission deliverables
Submit these files via Git (before the due time of this assignment):
- **Q1** and **Q2**: `parallelize_or_not.c`
- **Q2**: One slurm output file with the name pattern `slurm-XYZ-sequential.out`
- **Q2**: Four slurm output files with the name pattern `slurm-XYZ-ompsizeZYX.out`

Print, staple, write your name, and turn-in on Wednesday in class (or before
Thursday at my office, 474 McBryde):
- **Q4**: Printed report


## Prepare
See the continuously updated
[cheat sheet for TinkerCliffs](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md).
The following steps are short descriptions and more details are found when
clicking the link, which gets you to one section of the TinkerCliffs cheat
sheet.

- Open the [terminal](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#terminal)
- Connect via [ssh](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#ssh)
- After ssh to TinkerCliffs was successful, continue by going to the directory
  of your Git repository (assuming the directory is called most likely `cmda3634`):
```bash
cd ~/cmda3634
```
- **New Git Workflow:** [Switch branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#switch-branches) to `main`:
```bash
git checkout main
```
- [Git-fetch and merge](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#receive-changes-from-remote-jrudi) the files from the remote named `jrudi`
- Locate the files of this assignment:
```bash
cd H09
```
- List the content with `ls`;  The files you will need to work with are
```bash
Makefile
parallelize_or_not.c
submission_parallel.sh
submission_sequential.sh
```
- [Load the correct modules](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#modules) to set up the right compiler.

---

## Q1: Decide about parallelization (18 points)

### Background
A loop "runs efficiently in parallel" if its execution time with P threads
should be expected to be about 1/P times as long as with one thread while
leaving the effect/result of the loop unchanged.

### Instructions
Review the provided file `parallelize_or_not.c`. Understand what goes on in the
for-loop and in any functions that are called in the loop body. Compile and run
the code (in debugging mode) to show the output and understand how this output
was created with the code.

For each for-loop, the goal is to understand two things:
  1. Can the iterations of the loop run in parallel? -> **feasibility**
  2. Is the loop expected to run faster in parallel with more and more
      threads? -> **efficiency**

The task for each for-loop is to decide which of the following statements applies.

First, make a judgement about the **feasibility** of parallelization:
  - (a) The loop is able to run in parallel. It simply requires the
      OpenMP directive `#pragma omp parallel for`.
  - (b) The loop's code requires some modification or a more sophisticated pragma
      directive is needed before it can execute correctly in parallel.
      - In this case, explain why statement (a) is not true.
      - If the loop body requires significant changes for parallelization,
        describe the changes. The loop body modifications may require some
        cleverness or alternated arithmetics.
  - (c) The loop is impossible to parallelize.
      - In this case, explain what is preventing the parallelization of the loop.

Second, and if the first judgement was (a) or (b), make a judgement about the
**efficiency** of parallelization:
  - (a) The loop will run **efficiently** in parallel. It simply requires the
      OpenMP directive `#pragma omp parallel for`.
  - (b) The loop is expected to have difficulties scaling efficiently. Some
      modifications are needed to avoid "bottlenecks for scalability" (e.g.,
      improve load balancing)
      - In this case, explain why statement (a) is not true.
      - Describe the changes to the loop or additional OpenMP directives to
        improve the efficiency.
  - (c) The loop will not benefit from parallelization.
      - In this case, explain why the loop is a poor candidate for parallelization.

### Tasks
For each of the loops in `parallelize_or_not.c`, there is a total of nine
loops, carry out the decisions from the *Instructions* above.
Write your decisions as comments in the file `parallelize_or_not.c` above the
code of the loop. Example with dummy text as answers:
```c
  /* Loop 1
   * Set v[n] to the totient function of n */
  /*
     Answer, feasibility: 
     (a/b/c) The loop can/cannot be parallelized, because ...
     But the modifications ... and ... have to made.

     Answer, efficiency:
     (a/b/c) The loop has the same/different amout of work per iteration, because ...
     To make the parallelization efficient, we need to do ...
   */
  for (n=0; n<N; n++) {
    v[n] = totient(n);
  }
```

**Deliverable**: Comments in `parallelize_or_not.c`


## Q2: Implement parallelization (12 points)

From **Q1**, down-select all candidate loops that are feasible to parallelize
and where the parallelization can be done efficiently.
From these candidate loops, select **six** loops, where you implement the
parallelization with OpenMP.

For each loop that you do not parallelize, comment-out the loop.

**Deliverable**: Implementation in `parallelize_or_not.c`

### Before parallel implementation
Before you start changing the existing sequential code, run it once:
1. Make sure modules are loaded (see [load the correct modules](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#modules))
2. Compile with `make`
3. Run by submitting the script `submission_sequential.sh` to sbatch

**Deliverable** One slurm output file with the name pattern `slurm-XYZ-sequential.out`

### After parallel implementation
Before you parallelized six loops, run it in parallel:
1. Make sure modules are loaded (see [load the correct modules](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#modules))
2. Compile with `make`
3. Run by submitting the script `submission_parallel.sh` to sbatch

Having the slurm output file of the sequential run, compare if the results are
identical in the parallel runs. There will be four output files, one for a
different number of threads. Find and fix any bugs until the parallel runs
produce the same output as the sequential run.

**Deliverable** Four slurm output files with the name pattern `slurm-XYZ-ompsizeZYX.out`


## Q3: Submit results via Git into a new branch (5 points)

[Create a new branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#create-branches) in Git called `H09`:
```bash
git branch H09    # create a new branch named `H09`
git checkout H09  # switch to the new branch named `H09`
```

[Git add and commit](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#edit-add-commit-workflow)
the files that were previously listed as deliverables.

Double-check if your commit was successful, by looking at the output of 
```bash
git log -1  # for last commit
git log     # for all commits (scrollable with arrow keys; quit with `q`)
```

Upon successful commit, [push your changes](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#send-your-changes-eg-submit-homework)
```bash
git push origin H09  # tell Git to push the branch of the homework
```

After the `git push origin H09`, the changes will be uploaded to your Git
project at <https://code.vt.edu/>.  Double-check that these files actually made
it there:
1. Go to the webpage <https://code.vt.edu/>
2. Open a drop-down menu that has `main` selected (top-center of webpage), and
   select the branch `H09`
3. Locate the files you were supposed to push.  Click on a file to see that
   your changes were actually transferred.


## Q4: Intro video on programming for GPUs with CUDA (5 points)

Watch the following introduction video (duration 53 minutes):
<https://on-demand.gputechconf.com/gtc/2012/video/S0624-Monday-Introduction-to-CUDA-C.mp4>

Write-up in a report the following:
1. Summarize the content (in 1-3 sentences)
2. Explain what differences you noticed compared to OpenMP (in 1-3 sentences)
3. Explain how memory needs to be treated differently on GPUs compared to CPUs
4. Explain how to access elements of an array inside a "CUDA kernel"

**Deliverable**: Printed report
