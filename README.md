# Computer Science Foundations for CMDA (CMDA 3634 - CRN 82999 - Fall 2022)
- Class Time: MW 4:00pm-5:15pm
- Class Location: TORG 1040


## Main Resources of the Course
- [Syllabus & Course Policy](doc/syllabus.pdf)
- [Inclusion Guidelines](doc/inclusion_guidelines.pdf)
- [Lecture Notes](doc/lecture_notes.pdf) (Do not share with anyone! Do not upload anywhere!)


## Professor Contact Information
- Instructor: Dr. Johann Rudi (he/him/his)
- Email: <jrudi@vt.edu>
- Office: McBryde Room 474
- Office Hours: MT 11:00am-noon, W 1:00pm-2:00pm


## GTA Contact Information
- Name: Dan Folescu (he/him/his)
- Email: <dan1345@vt.edu>
- Office Hours: M 5:30pm-6:30pm
- Office Hours Location: CMDA learning center in the Old Security Building (OSB)


## ULA Office Hours
TBD


## CMDA Computing Consultants
The CMDA Computing Consultants are a team of CMDA majors who offer coding help to CMDA students. To check out their hours, locations, and other details about the Computing Consultants’ services, please visit their webpage.


## License
GNU General Public License v3.0

Permissions of this strong copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights.

For details, see the [LICENSE](LICENSE) file.
