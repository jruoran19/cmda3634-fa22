#include <stdio.h>
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

static void CUDA_CHKERR(cudaError_t err) {
  if (err != cudaSuccess) {
    printf("CUDA error (%d): %s\n", (int)err, cudaGetErrorString(err));
    fflush(stdout); // flush buffered print outputs
    exit(1);
  }
}

static void CURAND_CHKERR(curandStatus_t err) {
  if (err != CURAND_STATUS_SUCCESS) {
    printf("CURAND error (%d)\n", (int)err);
    fflush(stdout); // flush buffered print outputs
    exit(1);
  }
}

/* set type of state and, thus, the type of random number generator, for example:
 * - curandState
 * - curandStateMRG32k3a
 * - curandStatePhilox4_32_10_t
 */
typedef curandStateMRG32k3a util_curand_state_t;

__global__ void k_curand_setup_states(util_curand_state_t *globalState, uint64_t seed)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;

  curand_init(seed+tid, tid, 0, &globalState[tid]);
}

