#include <util_random.h>
#include <stdlib.h>
#include <math.h>

double random_uniform(void) {
  return rand () / (RAND_MAX + 1.0);
}

double random_normal(void) {
  double u, v, s;

  do {
    u = 2.0*(random_uniform () - 0.5); // uniform on [-1,1)
    v = 2.0*(random_uniform () - 0.5); // uniform on [-1,1)
    s = u*u + v*v;
  } while (s > 1.0 || s <= 0.0);

  s = sqrt(-2.0*log(s)/s);

  return u*s;
}

void generate_random_uniform_data(double *data, size_t length, double lo, double hi) {
  size_t i;

  for (i=0; i<length; i=i+1) {
    data[i] = (hi - lo)*random_uniform() + lo;
  }
}

void generate_random_normal_data(double *data, size_t length, double mean, double std) {
  size_t i;

  for (i=0; i<length; i=i+1) {
    data[i] = std*random_normal() + mean;
  }
}

void transform_normal_to_lognormal(double *data, size_t length) {
  size_t i;

  for (i=0; i<length; i=i+1) {
    data[i] = exp(data[i]);
  }
}
