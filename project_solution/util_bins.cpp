#include <util_bins.h>
#include <stdio.h>
#include <math.h>

/**
 * Sets up boundaries of bins.
 *
 * Input:
 *   n_bins       Number of bins (in one dimension)
 *   domain_min   Min value of first bin range
 *   domain_max   Max value of last bin range
 * 
 * Output:
 *   bin_ranges_1dim  Boundaries between bins (array size: `n_bins+1`)
 */
void setup_uniform_bin_ranges_per_dim(double *bin_ranges_1dim, int n_bins, double domain_min, double domain_max) {
  const double width = domain_max - domain_min;
  double x;
  int j;

  for (j=0; j<=n_bins; j++) {
    x = ((double) j) / ((double) n_bins);
    bin_ranges_1dim[j] = width * x + domain_min;
  }
}

void setup_variable_bin_ranges_per_dim(double *bin_ranges_1dim, int n_bins, double domain_min, double domain_max) {
  const double width = domain_max - domain_min;
  double x;
  int j;

  for (j=0; j<=n_bins; j++) {
    x = ((double) j) / ((double) n_bins);
    bin_ranges_1dim[j] = width * (2.0*pow(x - 0.5, 3) + 0.25 + 0.5*x) + domain_min;
  }
}

/**
 * Print out bin ranges.
 *
 * Input:
 *   bin_ranges       Boundaries between bins (array size: `dim*(n_bins_per_dim+1)`)
 *   dim              Number of dimensions
 *   n_bins_per_dim   Number of bins per dimension
 */
void print_bin_ranges(double *bin_ranges, int dim, int n_bins_per_dim) {
  int d, j;

  printf("<%s>\n", __func__);
  for (d=0; d<dim; d++) {
    printf("d=%d:", d);
    for (j=0; j<=n_bins_per_dim; j++) {
      printf(" %f", bin_ranges[d*(n_bins_per_dim+1) + j]);
    }
    printf("\n");
  }
  printf("</%s>\n", __func__);
}
