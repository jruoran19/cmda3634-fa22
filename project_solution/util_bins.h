#include <stddef.h>

void setup_uniform_bin_ranges_per_dim(double*, int, double, double);
void setup_variable_bin_ranges_per_dim(double*, int, double, double);
void print_bin_ranges(double*, int, int);
