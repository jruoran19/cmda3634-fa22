#include <util_test.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**
 * Tests conversion between bin indices and coodinates
 */
int util_test_conversion_between_index_and_coord(
    int dim, int n_bins_per_dim,
    void(*get_bin_coordinates_from_index)(int*,int),
    int(*get_bin_index_from_coordinates)(int*)) {

  const int n_bins_total = pow(n_bins_per_dim, dim);
  int bin_id, bin_id_from_coord;
  int coord[dim], coord_min[dim], coord_max[dim];
  int dim_id;
  int success, success_all = 1;

  for (dim_id=0; dim_id<dim; dim_id++) {
    coord_min[dim_id] = n_bins_total;
    coord_max[dim_id] = -1;
  }

  success = 1;
  for (bin_id=0; bin_id<n_bins_total; bin_id++) {
    get_bin_coordinates_from_index(coord, bin_id);
    bin_id_from_coord = get_bin_index_from_coordinates(coord);

    /* check if linear/flat index was recovered */
    if (bin_id_from_coord != bin_id) {
      success = 0;
      printf("Error in conversion between bin indices and coodinates:\n");
      printf("  input bin id = %d\n", bin_id);
      printf("  coodinates are:");
      for (int dim_id=0; dim_id<dim; dim_id++) { printf(" %d", coord[dim_id]); }
      printf("\n");
      printf("  bin id computed from coordinates = %d\n", bin_id_from_coord);
    }

    /* update coordinate min/max */
    for (dim_id=0; dim_id<dim; dim_id++) {
      if (coord[dim_id] < coord_min[dim_id])  coord_min[dim_id] = coord[dim_id];
      if (coord_max[dim_id] < coord[dim_id])  coord_max[dim_id] = coord[dim_id];
    }
  }
  if (!success)  success_all = 0;

  /* check coordinate min/max */
  success = 1;
  for (dim_id=0; dim_id<dim; dim_id++) {
    if (0 != coord_min[dim_id] || coord_max[dim_id] != (n_bins_per_dim - 1)) {
      success = 0;
      break;
    }
  }
  if (!success) {
    printf("Error in conversion from bin indices to coodinates, because the min (or max) coordinates are not zero (or not n_bins_per_dim-1=%d).\n", n_bins_per_dim-1);
    printf("  min coodinates that were reached are:");
    for (int dim_id=0; dim_id<dim; dim_id++) { printf(" %d", coord_min[dim_id]); }
    printf("\n");
    printf("  max coodinates that were reached are:");
    for (int dim_id=0; dim_id<dim; dim_id++) { printf(" %d", coord_max[dim_id]); }
    printf("\n");
  }
  if (!success)  success_all = 0;

  return success_all;
}

