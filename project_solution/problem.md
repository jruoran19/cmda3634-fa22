# Project (100 points)


## Honor Code:
By submitting this project, you acknowledge that you have adhered to the
Virginia Tech Honor Code and attest to the following:
> I have neither given nor received unauthorized assistance on this project.
> The work I am presenting is ultimately my own.

### Using Google search
It is allowed to use Google to search for the meaning of compiler errors &
warnings, explanation for Git messages, or any sort of message that you are
being shown in the terminal.  Using Google skillfully to improve your
understanding of a subject is an important skill in programming.


## Submission

### Submission deliverables (all required to arrive on time in order to get graded)
Submit these files via Git before their due time:
- at code.vt.edu: all files to run the code, contained in the folder `project_solution`
- on Canvas: 3 files of slides presenting each of the 3 project phases

### Submission slides
Each of the project's three phases require one file of presentation slides
(created with PowerPoint or similar tools).  For one deck of presentation
slides, describe the following
- the problem you are trying to solve
- the key idea(s) for the problem's solution
  (example: mathematical equations, explanations)
- the description of your solution (do not cut-and-paste code, but pseudo-codes
  are ok)

Use 2-3 slides per deck (not including a title slide, which is optional). Have
your name on every slide.


## Deadlines
- Last day of classes: Deadline until when answers to questions will be given.
- Last day of finals (grace period): Deadline until when all files must be
  submitted. No questions will be answered during this grace period.


## Notes for the project
All the files of your solution code and your scripts need to be in the folder
`project_solution`.

For calls to `cudaXYZ` functions (e.g., `cudaMalloc`, `cudaFree`, `cudaMemcpy`)
check for CUDA errors with `CUDA_CHKERR` as in the following example:
```
CUDA_CHKERR( cudaMalloc(...) );
```
This will notify you of errors when running the code. It's very helpful for debugging.


## Prepare (only once)
See the continuously updated
[cheat sheet for ARC clusters](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md).
The following steps are short descriptions and more details are found when
clicking the links, which gets you to one section of the cheat sheet.

- Open the [terminal](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#terminal)
- Connect via ssh to ARC's Infer cluster:
  ```bash
  ssh [PID]@infer1.arc.vt.edu
  ```
- After ssh to Infer was successful, continue by going to the directory
  of your Git repository (assuming the directory is called most likely `cmda3634`):
  ```bash
  cd ~/cmda3634
  ```
- [Switch branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#switch-branches) to `main`:
  ```bash
  git checkout main
  ```
- [Git-fetch and merge](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#receive-changes-from-remote-jrudi) the files from the remote named `jrudi`
- Copy the files that are provided for you to your solution folder. That means
  copy files from `project_problem` to `project_solution`:
  ```bash
  cp project_problem/* project_solution/
  ```

---

## Background: Monte Carlo integration in arbitrary dimensions

This project is about Monte Carlo integration. It was the topic of Homework 08
and more background information can be found in [H08's problem
statement](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/H08/H08_problem.md#first-understand-the-background).

Monte Carlo integration is especially effective in high dimensions, which is
when the domain over which we integrate is larger than 3.  Therefore, we will
implement Monte Carlo integration in arbitrary dimensions.

In H08, the integral of `f(x)` was approximated by:
```
int_approx = (1/n_mc_evals) * SUM_{i IN [0,n_mc_evals)} f(x)/p(x)
```
where
- `int_approx` stores the result (output)
- `n_mc_evals` is the number of evaluations/samples
- `f(x)` is coming from the function `integrand_fn(x)`
- `x` is randomly selected (i.e., sampled)
- `p(x)` was a probability density function

In this project, we approximate the integral differently using bins (like bins
of a histogram):
```
int_approx = (1/n_mc_evals) *
             SUM_{bin_id IN [0,n_bins_total)} ( n_bins_total*bin_volume * SUM_{j IN [0,n_bin_evals)} f(x) )
```
where
- `int_approx` stores the result (output)
- `n_mc_evals` is the number of evaluations/samples
- `n_bins_total` is the total number bins and equal to `n_bins_per_dim^dim`
- `bin_id` is the index (single integer) of a bin
- `bin_volume` is the volume of the bin with index `bin_id`
- `n_bin_evals` is the number of evaluations/samples per bin

Note that `n_bins_total*bin_volume` plays the role of the density `p(x)` from before.

The new aspect here, compared to H08, is the two nested sums, while the number
of evaluations/samples is kept the same, that means:
```
n_mc_evals = n_bin_evals * n_bins_total
```

The bins in the formula for `int_approx` are what we will use as independent
units of work when we turn to parallelization.

---

## Existing code templates provided for you

First, familiarize yourself with the existing code to see the scope of the work
and how individual pieces will fit together.

### File `mc_int.cu`

This is where most (or all) of your code implementation goes.

There are some important parameters that are set in this file. It means that
these parameters are "baked" into the compiled program. Use them as instructed
in the comments. Your final solution needs to work for all of the listed values
of those parameters.

```c
/* set the dimension of the domain over which the integration is performed
 * TODO use all of these dimensions to test your code:
 * - 1
 * - 2
 * - 4
 */
#define DIM 1
```

```c
/* set the number bins per dimension
 * TODO set according to:
 * - use `8` for development and debugging
 * - use `64` for "real" calculations after debugging
 */
#define N_BINS_PER_DIM 8
```

### File `submission.sh`

This is where you adjust the max runtime to request and the number of
evaluations/samples.

```bash
# run the program
# TODO set the total number of MC evaluations/samples:
# - use `300200100` for developement and debugging (runtime is ~30 seconds, so set -t 00:01:00)
# - use `4300200100` for "real" calculations after debugging (runtime is <10 minutes, so set -t 00:15:00)
./mc_int 300200100
```

---

## Phase 1: Implement helper functions (25 points)

**Deliverable:** Code in folder `project_solution` and 2-3 presentation slides.

Implement the helper functions that are located toward the beginning of the
file `mc_int.cu`.  The functions that need implementation have `TODO` comments.
The explanation of the functions are in the comment sections right above each
function declaration.

**The helper functions should make sense when seeing their role in the whole
context of the program.**

### Function `get_bin_index_from_coordinates`

Input:
```
  coord    Multi-dimensonal coordinates, integer-valued (array size is `DIM`)
```

Task: Calculate the bin index from a bin's multi-dimensional coordinates.

Return:
```
  Integer valued index corresponding to one bin (range: 0 <= bin_id < N_BINS_PER_DIM^DIM)
```

To start developing the solution mathematically, think of 2 dimensions and how
to arrange the elements of a matrix of size `n*n`,
```
a_0,0    a_0,1    ... a_0,n-1
a_1,0    a_1,1    ... a_1,n-1
:        :            :
a_n-1,0  a_n-1,1  ... a_n-1,n-1
```
into a single line:
```
a_0,0, a_0,1, ..., a_0,n-1, a_1,0, a_1,1, ..., a_1,n-1, ..., ..., a_n-1,0, a_n-1,1, ..., a_n-1,n-1
```
Now the indices of that single line would be:
```
    0,     1, ...,     n-1,   n  ,   n+1, ...,    2n-1, ..., ...,  (n-1)n, (n-1)n+1, ..., n^2-1
```

So, if the input coordinates (in 2 dimensions) are `coord[2]={i,j}` for some arbitrary
`0<=i,j<n`, then the formula to deduce from above is
```
bin_id = n*i + j
```

Now you need to derive the formula for arbitrary dimensions `d`. In that case, the
input coordinates are `coord[d]={i_0, i_1, ..., i_n-1}`. What is the `bin_id`?
Hint: `bin_id` should be in the range `0 <= bin_id < n^d`

### Function `get_bin_coordinates_from_index`

Input:
```
  bin_id   Integer valued index corresponding to one bin (range: 0 <= bin_id < N_BINS_PER_DIM^DIM)
```

Task: Calculate multi-dimensional coordinates the input bin index.
This reverses the function `get_bin_index_from_coordinates`.

Output:
```
  coord    Multi-dimensonal coordinates, integer-valued (array size is `DIM`)
```

### Function `get_bin_volume`

Input:
```
  bin_min  Min values of the bin's bounding box (array size is `DIM`)
  bin_max  Max values of the bin's bounding box (array size is `DIM`)
```

Task: Calculate the volume of a bin from its bounding box.

Return:
```
  Volume of one bin (as double type).
```

Hint the "volume" of a 2 dimensional bin box would be
```
bin_volume = (bin_max[0] - bin_min[0])*(bin_max[1] - bin_min[1])
```
Now you need to derive the formula for arbitrary dimensions `d`.

---

## Phase 2: Implement `mc_int_epoch_sequential` (30 points)

**Deliverable:** Code in folder `project_solution` and 2-3 presentation slides.

This function needs to run on the CPU and does **not** have any parallelization.

Input:
```
  n_mc_evals   Number of Monte Carlo evaluations (i.e., samples)
  n_bin_evals  Number of Monte Carlo evaluations per bin
  bin_ranges   Boundaries between bins (array size: `DIM*(N_BINS_PER_DIM+1)`)
```

Output:
```
  int_approx  Pointer(!) to Approximate value of the integration
  var_approx  Pointer(!) to Approximate value of the variance (i.e., error^2 of integration)
  bin_content Values of integration for each bin (array size: `N_BINS_PER_DIM^DIM`)
```

The approximate value of the integration is
```
int_approx = (1/n_mc_evals) *
             SUM_{bin_id IN [0,n_bins_total)} ( n_bins_total*bin_volume * SUM_{j IN [0,n_bin_evals)} f(x) )
```

The approximate value of the variance (i.e., error^2 of integration) is computed by first
```
avg2 = (1/n_mc_evals) *
       SUM_{bin_id IN [0,n_bins_total)} ( n_bins_total*bin_volume * SUM_{j IN [0,n_bin_evals)} f(x)*f(x) )
```
(the difference is `f(x)*f(x)`), and then
```
var_approx = avg2/(n_mc_evals - 1) - int_approx^2/(n_mc_evals - 1)
```

The values of the output array `bin_content` are storing intermediate results
from computing `int_approx` (this will be important later for GPU code); the
values are
```
bin_content[bin_id] = ( n_bins_total*bin_volume * SUM_{j IN [0,n_bin_evals)} f(x) )
```

Inside the loops of the function `mc_int_epoch_sequential`, use the helper
functions from above for each `bin_id` in this way:
```c
/* get bin parameters */
get_bin_coordinates_from_index(coord, bin_id);            // computes coordinates from a bin index
get_bin_min_and_max(bin_min, bin_max, bin_ranges, coord); // computes a bin's min & max, using the coordinates
bin_volume = get_bin_volume(bin_min, bin_max);            // computes a bin's volume
```
declare the needed arrays of size `DIM` at the top of the function as:
```c
int coord[DIM];
double bin_min[DIM], bin_max[DIM];
```

Use the provided functions for random number generation and integrand evaluation:
```c
random_fn(x, bin_min, bin_max);
f = integrand_fn(x);
```
declare the needed array of size `DIM` at the top of the function as:
```c
double x[DIM];
```

---

## Phase 3: Implement `k_mc_int_epoch` (40 points)

**Deliverable:** Code in folder `project_solution` and 2-3 presentation slides.

Once the sequential CPU version works in all dimensions, parallelize it using
the CUDA API.

The CUDA kernel `k_mc_int_epoch` implements the calculation needed for just one bin:
```
bin_content[bin_id] = ( n_bins_total*bin_volume * SUM_{j IN [0,n_bin_evals)} f(x) )
```
Then to goal is to call this kernel function (in the `main` function) with as
many (or more) threads such that every bin's result is calculated and stored in
the array `d_bin_content` on the device.

The final summing of the bin contents is done with a CPU function
`reduce_bin_content_to_integral` that is already provided.

Finally, finish the CUDA code in the `main` function. So you need to allocate
memory on the GPU device and copy memory between host and device. Think about
which memory is input and which output to figure out what memory transfer is
needed.

---

## Phase 4: Create final slurm output for submission (5 points)

**Deliverable:** One slurm output file in `project_solution`.

The goal is to match (or improve upon) the accuracy of results and the
wall-clock times from a reference implementation. These references are provided
in a slurm output file, called `slurm-reference.out`, together with the problem
statement (in folder `project_problem`).

---

## Extra credit

### X01 (5 points)
**Background:**
For two integers `a` and `b`, the division of these integers in C, namely
`a/b`, is identical to the [floor operation](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions):
```
  a/b = floor(a/b)
```
There is no such convenience for the ceiling operation.

We have been exploiting the behavior of integer division to calculate how many
CUDA blocks are needed.  Given problem size `N` and the number of threads per
block `P`, the block count is: `ceiling(N/P)`.
Instead we have used the formula `floor((N+P-1)/P)`.

**Problem:**
For any given positive integers `N` and `P`, proof the identity
```
ceiling(N/P) = floor((N+P-1)/P)
```

**Deliverable:** PDF file uploaded to Canvas with the proof (typed or hand-written).

### X02 (5 points)
Implement the reduction carried out in the CPU-only function
`reduce_bin_content_to_integral` in CUDA.
The [lecture notes](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/doc/lecture_notes.pdf)
have a chapter on this topic

> Chapter 23
>
> CUDA part 2: GPU sum reduction

on page 327.

**Deliverable:** Additional function code in the file `mc_int.cu`. Additional file with 2-3 presentation slides.

---

## Submit solution files via Git into a new branch (required to get graded)

[Create a new branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#create-branches) in Git called `project`:
```bash
git branch project    # create a new branch named `project`
git checkout project  # switch to the new branch named `project`
```

[Git add and commit](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#edit-add-commit-workflow)
the files that were previously listed as deliverables.

Double-check if your commit was successful, by looking at the output of 
```bash
git log -1  # for last commit
git log     # for all commits (scrollable with arrow keys; quit with `q`)
```

Upon successful commit, [push your changes](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#send-your-changes-eg-submit-homework)
```bash
git push origin project  # tell Git to push this particular branch
```

After the `git push origin project`, the changes will be uploaded to your Git
project at <https://code.vt.edu/>.  Double-check that these files actually made
it there:
1. Go to the webpage <https://code.vt.edu/>
2. Open a drop-down menu that has `main` selected (top-center of webpage), and
   select the branch `project`
3. Locate the files you were supposed to push.  Click on a file to see that
   your changes were actually transferred.
