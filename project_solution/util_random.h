#include <stddef.h>

double random_uniform(void);
double random_normal(void);

void random_uniform_data(double*, size_t, double, double);
void random_normal_data(double*, size_t, double, double);

void random_transform_normal_to_lognormal(double*, size_t);
