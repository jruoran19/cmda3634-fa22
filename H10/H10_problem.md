# Assigment 10 (32 points)


## Honor Code:
By submitting this assignment, you acknowledge that you have adhered to the
Virginia Tech Honor Code and attest to the following:
> I have neither given nor received unauthorized assistance on this assignment.
> The work I am presenting is ultimately my own.

### Using Google search
It is allowed to use Google to search for the meaning of compiler errors &
warnings, explanation for Git messages, or any sort of message that you are
being shown in the terminal.  Using Google skillfully to improve your
understanding of a subject is an important skill in programming.


## Submission
- The online submission of this homework is under your own and private Git
  project at: <https://code.vt.edu>
- The offline submission of this homework is in class or at the office 474
  McBryde. Slide the hard-copy under the door if it's closed (Note that the
  McBryde building has automatically locking doors that lock sometime in the
  evening.)
- You do not need to submit anything to Canvas.

### Submission deliverables
Submit these files via Git (before the due time of this assignment):
- **Q1**: `axpy_cpu.c`
- **Q2**: `axpy_gpu.cu`
- **Q3**: Two slurm output files

Print, staple, write your name, and turn-in on Wednesday in class (or before
Thursday at my office, 474 McBryde):
- **Q4**: Printed report


## New for this homework

**Don't** ssh to TinkerCliffs.

Instead ssh to Infer (<infer1.arc.vt.edu>).


## Prepare
See the continuously updated
[cheat sheet for ARC clusters](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md).
The following steps are short descriptions and more details are found when
clicking the link, which gets you to one section of the cheat sheet.

- Open the [terminal](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#terminal)
- Connect via ssh to ARC's Infer cluster:
  ```bash
  ssh [PID]@infer1.arc.vt.edu
  ```
- After ssh to Infer was successful, continue by going to the directory
  of your Git repository (assuming the directory is called most likely `cmda3634`):
  ```bash
  cd ~/cmda3634
  ```
- **New Git Workflow:** [Switch branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#switch-branches) to `main`:
  ```bash
  git checkout main
  ```
- [Git-fetch and merge](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#receive-changes-from-remote-jrudi) the files from the remote named `jrudi`
- Locate the files of this assignment:
  ```bash
  cd H10
  ```
- List the content with `ls`;  The files you will need to work with are
  ```bash
  axpy_cpu.c
  axpy_gpu.cu
  Makefile_cpu
  Makefile_gpu
  setup_env.sh
  submission_cpu.sh
  submission_gpu.sh
  ```
- [Load the correct modules](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#modules) to set up the right compiler.

---

## Q1: Implement CPU code (10 points)

**Deliverable**: Code in `axpy_cpu.c`

### Q1.A (4 points)
Implement the function `saxpy_increment_1` that computes the "AXPY" (A times X Plus Y),
that is
```
  y_i = a * x_i + y_i
```
for `0 <= i < N`, where `N` is a given problem size, and `x_i`, `y_i` are
entries of a vector with dimension `N`.

The goal is implement the AXPY using consecutive memory access for each thread.

This is done with the following pseudo code:
```
FUNCTION: saxpy_increment_1
INPUT:  n, a, x[n], y[n]
OUTPUT: y[n]

OMP PARALLEL:
  index_begin     = (n*ompthread)/ompsize;
  index_end       = (n*(ompthread + 1))/ompsize;
  index_increment = 1;

  FOR i IN [index_begin,index_end) INCREMENT BY index_increment
    y[i]= a * x[i] + y[i]
```
where `ompsize` is the number of OpenMP threads and `ompthread` is the thread
index of an OpenMP thread.

### Q1.B (4 points)
Implement the function `saxpy_increment_ompsize` that computes the "AXPY".

The goal is implement the AXPY using non-consecutive memory access for each
thread by incrementing with ompsize.

This is done with the following pseudo code:
```
FUNCTION: saxpy_increment_ompsize
INPUT:  n, a, x[n], y[n]
OUTPUT: y[n]

OMP PARALLEL:
  index_begin     = ompthread
  index_end       = n
  index_increment = ompsize

  FOR i IN [index_begin,index_end) INCREMENT BY index_increment
    y[i]= a * x[i] + y[i]
```
where `ompsize` is the number of OpenMP threads and `ompthread` is the thread
index of an OpenMP thread.

### Q1.C (2 points)
Implement the memory allocation and deallocation in the `main` function.
- allocate float-arrays `h_x` and `h_y`, having `N` entries, using `malloc()`
- deallocate `h_x` and `h_y` using `free()`


## Q2: Implement GPU code (12 points)

**Deliverable**: Code in `axpy_gpu.cu`

### Q2.A (4 points)
Implement the CUDA kernel function `k_saxpy_increment_1` that computes the
"AXPY" (A times X Plus Y).

The goal is implement the AXPY using consecutive memory access for each thread.

This is done with the following pseudo code:
```
FUNCTION: k_saxpy_increment_1
INPUT:  n, a, x[n], y[n]
OUTPUT: y[n]

index_begin     = (n*thread_id)/n_threads;
index_end       = (n*(thread_id + 1))/n_threads;
index_increment = 1;

FOR i IN [index_begin,index_end) INCREMENT BY index_increment
  y[i]= a * x[i] + y[i]
```
where 
- `thread_id` is the thread index computed as `blockIdx.x * blockDim.x + threadIdx.x`
- `n_threads` is the number of threads computed as `blockDim.x * gridDim.x`

### Q2.B (4 points)
Implement the function `k_saxpy_increment_dim` that computes the "AXPY".

The goal is implement the AXPY using non-consecutive memory access for each
thread by incrementing with the total number of threads.

This is done with the following pseudo code:
```
FUNCTION: k_saxpy_increment_dim
INPUT:  n, a, x[n], y[n]
OUTPUT: y[n]

index_begin     = thread_id
index_end       = n
index_increment = n_threads

FOR i IN [index_begin,index_end) INCREMENT BY index_increment
  y[i]= a * x[i] + y[i]
```
where `n_threads` is the number of CUDA threads and `thread_id` is the thread
index of a CUDA thread, as given in **Q2.A**.

### Q2.C (4 points)
Implement the memory allocation and deallocation in the `main` function.
- allocate float-arrays in host memory `h_x` and `h_y`, having `N` entries, using `malloc()`
- allocate float-arrays in device memory `d_x` and `d_y`, having `N` entries, using `cudaMalloc()`
- deallocate host memory `h_x` and `h_y` using `free()`
- deallocate device memory `d_x` and `d_y` using `cudaFree()`

Implement the memory the memory transfers between host and device in the `main` function.
- transfer arrays `*_x` and `*_y` from host memory to device memory with `cudaMemcpy()`, that is
  - transfer `h_x` to `d_x`
  - transfer `h_y` to `d_y`
- transfer only array `*_y` back from device memory to host memory with `cudaMemcpy()`, that is
  - transfer `d_y` to `h_y`


## Q3: Compile and run (5 points)

Before compiling, make sure that you loaded the modules with the provided script:
```bash
source setup_env.sh
```

### Q3.A: CPU code (2.5 points)
Compile the CPU code with:
```bash
make -f Makefile_cpu
```

Run the CPU program with:
```bash
sbatch submission_cpu.sh
```

The slurm output file contains output corresponding to the functions
`saxpy_increment_ompsize` and `saxpy_increment_1`.  For each function an error
is computed, which needs to be below $10^{-7}$, and a wall clock time.

The output is repeated for multiple numbers of OpenMP threads, namely 1, 2, 4, 8, 16, 28.

**Deliverable**: slurm output file `slurm-XYZ-cpu.out`

### Q3.B: GPU code (2.5 points)
Compile the GPU code with:
```bash
make -f Makefile_gpu
```

Run the GPU program with:
```bash
sbatch submission_gpu.sh
```

The slurm output file contains output corresponding to the functions
`saxpy_increment_dim` and `saxpy_increment_1`.  For each function an error
is computed, which needs to be below $10^{-7}$.

Below the error output, is output generated from the profiler NVPROF (see
"Profiling result" in slurm output file).  This profiling result lists the time
for the kernel functions `saxpy_increment_dim` and `saxpy_increment_1`.

**Deliverable**: slurm output file `slurm-XYZ-gpu.out`


## Q4: Report about results in Q3 (5 points)

Write a report about the timing extracted from the slurm output file that was
generated in **Q3.A**.
Report the wall clock times per call (in milliseconds) for the AXPY function calls
and for OpenMP thread counts 1, 2, 4, 8, 16, and 28.
Create a table with the following headers:
```
| #threads | saxpy_increment_ompsize, time [ms] | saxpy_increment_1, time [ms] |
```
Then create one row for each number of threads printed in the slurm output.

Add to the report the timing extracted from the slurm output file that was
generated in **Q3.B**.
Report the wall clock times per call (in milliseconds) for the AXPY function calls.
Create a table with the following headers:
```
| saxpy_increment_dim, time [ms] | saxpy_increment_1, time [ms] |
```
Then create a single row for the timing on the GPU.

Finally, write the observations that you make from these two tables.  What are
the trends between the two AXPY implementations on the CPU and on the GPU?
Create a hypothesis why you think the times between the different AXPY
implementations differ; and support the hypothesis with arguments as well as you can.

**Deliverable**: Printed report


## Q5: Submit results via Git into a new branch (5 points)

[Create a new branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#create-branches) in Git called `H10`:
```bash
git branch H10    # create a new branch named `H10`
git checkout H10  # switch to the new branch named `H10`
```

[Git add and commit](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#edit-add-commit-workflow)
the files that were previously listed as deliverables.

Double-check if your commit was successful, by looking at the output of 
```bash
git log -1  # for last commit
git log     # for all commits (scrollable with arrow keys; quit with `q`)
```

Upon successful commit, [push your changes](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#send-your-changes-eg-submit-homework)
```bash
git push origin H10  # tell Git to push the branch of the homework
```

After the `git push origin H10`, the changes will be uploaded to your Git
project at <https://code.vt.edu/>.  Double-check that these files actually made
it there:
1. Go to the webpage <https://code.vt.edu/>
2. Open a drop-down menu that has `main` selected (top-center of webpage), and
   select the branch `H10`
3. Locate the files you were supposed to push.  Click on a file to see that
   your changes were actually transferred.
