#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <math.h>

/* set the problem sizes */
#define PROBLEM_SIZE 1200300400

/**
 * CUDA kernel function: Computes AXPY (A times X Plus Y) in 
 * single precision (32-Bit) arithmetic.
 * Use consecutive memory access for each thread.
 */
__global__ void k_saxpy_increment_1(size_t n, float a, float *x, float *y) {
  int n_threads = blockDim.x * gridDim.x;
  int thread_id = blockIdx.x*blockDim.x+threadIdx.x;
  int index_begin = (n*thread_id)/n_threads;
  int index_end = (n*(thread_id + 1))/n_threads;
  for (int i = index_begin; i < index_end; i++)
  {
    y[i]=a*x[i]+y[i];
  }
}

/**
 * CUDA kernel function: Computes AXPY (A times X Plus Y) in 
 * single precision (32-Bit) arithmetic.
 * Use non-consecutive memory access for each thread by incrementing with blockDim*gridDim.
 */
__global__ void k_saxpy_increment_dim(size_t n, float a, float *x, float *y) {
  int n_threads = blockDim.x * gridDim.x;
  int thread_id = blockIdx.x*blockDim.x+threadIdx.x;
  int index_begin = thread_id;
  int index_end = n;
  for (int i = index_begin; i < index_end; i+=n_threads)
  {
    y[i]=a*x[i]+y[i];
  }
}

/**
 * Main function.
 */
int main(int argc, char **argv) {
  const size_t N = PROBLEM_SIZE;
  float *h_x=NULL, *h_y=NULL; // arrays for host memory (access with CPU)
  float *d_x=NULL, *d_y=NULL; // arrays for device memory (access with GPU)
  size_t i;
  float max_error;

  /* allocate arrays in host memory (CPU) */
  // allocate `h_x` and `h_y`, having `N` entries, using `malloc()`
  h_x = (float*)malloc(sizeof(float)*N);
  h_y = (float*)malloc(sizeof(float)*N);

  /* allocate arrays in device memory (GPU) */
  // allocate `d_x` and `d_y`, having `N` entries, using `cudaMalloc()`
  cudaMalloc(&d_x, sizeof(float)*N);
  cudaMalloc(&d_y, sizeof(float)*N);

  printf("========================================\n");
  printf("N = %lu\n", (long unsigned int) N);
  fflush(stdout); // flush buffered print outputs

  if (!h_x || !h_y || !d_x || !d_y) {  // if previous TODOs incomplete
    fprintf(stderr, "Allocation TODOs were not completed!\n");
    return 1;
  }

  /* initialize x and y arrays (CPU) */
  for (i=0; i<N; i++) {
    h_x[i] = 1.3;
    h_y[i] = 2.4;
  }

  /* transfer data from host to device memory (CPU->GPU) */
  // transfer arrays `*_x` and `*_y` from host memory to device memory with `cudaMemcpy()`
  cudaMemcpy(d_x, h_x, sizeof(float)*N, cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, h_y, sizeof(float)*N, cudaMemcpyHostToDevice);

  /* run the kernel function with 100 x 256 threads (GPU) */
  k_saxpy_increment_1<<< 100, 256 >>>(N, 1.0, d_x, d_y);
  /* wait for GPU threads to complete */
  cudaDeviceSynchronize();

  /* transfer data from device to host memory (GPU->CPU) */
  // transfer only array `*_y` from device memory to host memory with `cudaMemcpy()`
  cudaMemcpy(h_y, d_y, sizeof(float)*N, cudaMemcpyDeviceToHost);

  /* check errors (CPU) */
  max_error = 0.0;
  for (i=0; i<N; i++) {
    max_error = fmax(max_error, fabs(h_y[i]-3.7));
  }
  printf("----------------------------------------\n");
  printf("saxpy_increment_1\n");
  printf("  Max error = %e\n", max_error);

  /* run the kernel function with 100 x 256 threads (GPU) */
  k_saxpy_increment_dim<<< 100, 256 >>>(N, 1.0, d_x, d_y);
  /* wait for GPU threads to complete */
  cudaDeviceSynchronize();

  /* transfer data from device to host memory (GPU->CPU) */
  // transfer only array `*_y` from device memory to host memory with `cudaMemcpy()`
  cudaMemcpy(h_y, d_y, sizeof(float)*N, cudaMemcpyDeviceToHost);

  /* check errors (CPU) */
  max_error = 0.0;
  for (i=0; i<N; i++) {
    max_error = fmax(max_error, fabs(h_y[i]-5.0));
  }
  printf("----------------------------------------\n");
  printf("saxpy_increment_dim\n");
  printf("  Max error = %e\n", max_error);

  printf("========================================\n");
  fflush(stdout); // flush buffered print outputs

  /* deallocate arrays in host memory (CPU) */
  // deallocate `h_x` and `h_y`
  free(h_x);
  free(h_y);

  /* deallocate arrays in device memory (GPU) */
  // deallocate `d_x` and `d_y`
  cudaFree(d_x);
  cudaFree(d_y);

  return 0;
}
