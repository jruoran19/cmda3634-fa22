#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

/* set the problem sizes */
#define PROBLEM_SIZE 1200300400

/**
 * Computes AXPY (A times X Plus Y) in single precision (32-Bit) arithmetic.
 * Use consecutive memory access for each thread.
 */
void saxpy_increment_1(size_t n, float a, float *x, float *y) {
  int ompsize;

  #pragma omp parallel shared(ompsize)
  {
    ompsize = omp_get_num_threads();
  }

  int ompthread;

  #pragma omp parallel shared(n, a, x, y, ompsize) private(ompthread)
  {
    ompthread = omp_get_thread_num();
    int index_begin = (n*ompthread)/ompsize;
    int index_end = (n*(ompthread + 1))/ompsize;
    for (int i = index_begin; i < index_end; i++)
    {
      y[i]=a*x[i]+y[i];
    }
  }
}

/**
 * Computes AXPY (A times X Plus Y) in single precision (32-Bit) arithmetic.
 * Use non-consecutive memory access for each thread by incrementing with ompsize.
 */
void saxpy_increment_ompsize(size_t n, float a, float *x, float *y) {
  int ompsize;

  #pragma omp parallel shared(ompsize)
  {
    ompsize = omp_get_num_threads();
  }

  int ompthread;

  #pragma omp parallel shared(n, a, x, y, ompsize) private(ompthread)
  {
    ompthread = omp_get_thread_num();
    int index_begin = ompthread;
    int index_end = n;
    for (int i = index_begin; i < index_end; i+=ompsize)
    {
      y[i]=a*x[i]+y[i];
    }
  }
}

int main(int argc, char **argv){
  const size_t N = PROBLEM_SIZE;
  float *h_x=NULL, *h_y=NULL; // arrays for host memory (access with CPU)
  size_t i;
  float max_error;
  int ompsize;
  double time_begin, time_end;

  #pragma omp parallel shared(ompsize)
  {
    #pragma omp master
    ompsize = omp_get_num_threads();
    #pragma omp barrier
  }

  /* allocate arrays in host memory (CPU) */
  // allocate `h_x` and `h_y`, having `N` entries, using `malloc()`
  h_x = (float*)malloc(sizeof(float)*N);
  h_y = (float*)malloc(sizeof(float)*N);

  printf("========================================\n");
  printf("  N = %lu, P = %d\n", (long unsigned int) N, ompsize);
  fflush(stdout); // flush buffered print outputs

  if (!h_x || !h_y) {  // if previous TODOs incomplete
    fprintf(stderr, "Allocation TODOs were not completed!\n");
    return 1;
  }

  /* initialize x and y arrays (CPU) */
  for (i=0; i<N; i++) {
    h_x[i] = 1.3;
    h_y[i] = 2.4;
  }

  /* run axpy (CPU) */
  time_begin = omp_get_wtime();
  saxpy_increment_1(N, 1.0, h_x, h_y);
  time_end = omp_get_wtime();

  /* check errors (CPU) */
  max_error = 0.0;
  for (i=0; i<N; i++) {
    max_error = fmax(max_error, fabs(h_y[i]-3.7));
  }
  printf("----------------------------------------\n");
  printf("saxpy_increment_1\n");
  printf("  Max error = %e\n", max_error);
  printf("  Wall time per call [ms] ~ %g\n", (time_end - time_begin)*1000.0);

  /* run axpy (CPU) */
  time_begin = omp_get_wtime();
  saxpy_increment_ompsize(N, 1.0, h_x, h_y);
  time_end = omp_get_wtime();

  /* check errors (CPU) */
  max_error = 0.0;
  for (i=0; i<N; i++) {
    max_error = fmax(max_error, fabs(h_y[i]-5.0));
  }
  printf("----------------------------------------\n");
  printf("saxpy_increment_ompsize\n");
  printf("  Max error = %e\n", max_error);
  printf("  Wall time per call [ms] ~ %g\n", (time_end - time_begin)*1000.0);

  printf("========================================\n");
  fflush(stdout); // flush buffered print outputs

  /* deallocate arrays in host memory (CPU) */
  // deallocate `h_x` and `h_y` using `free()`
  free(h_x);
  free(h_y);

  return 0;
}
