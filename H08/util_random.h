#include <stddef.h>

void random_initialize(int);
void random_finalize(void);

double random_uniform(int);
double random_normal(int);

void random_uniform_data(double *data, size_t length, double lo, double hi);
void random_normal_data(double *data, size_t length, double mean, double std);

void random_transform_normal_to_lognormal(double *data, size_t length);
