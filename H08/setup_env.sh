#!/bin/bash

# Help: Run this script as: source setup_env.sh

# set modules to system's default
module reset

# load compilers
#module load gompi
module load foss/2020a
#module load intel/2019b

# load GSL
module load GSL/2.6-GCC-9.3.0
