#include <stddef.h>

void test_find_bins(float*, int, float*, size_t,
                    void(*)(int*, float*, int, float*, size_t),
                    void(*)(int*, float*, int, float*, size_t));

void test_pi_approx(size_t, double(*)(size_t));

void test_mc_integral(size_t, void(*)(double*,double*,size_t));
