#include <util_random.h>
#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <omp.h>

gsl_rng **util_random_gsl_rng = NULL;
int     n_util_random_gsl_rng = 0;

/**
 * Initializes the random number generator.
 *
 * For the rng_type, use one of:
 * - gsl_rng_default
 * - gsl_rng_taus
 * - gsl_rng_mt19937
 * - gsl_rng_ranlxd1
 * - gsl_rng_cmrg
 * - gsl_rng_gfsr4
 */
void random_initialize(int ompsize) {
  const gsl_rng_type *rng_type = gsl_rng_gfsr4;
  const int seed = 363422;  // fix a random seed for reproducability
  int ti;

  gsl_rng_env_setup();
  n_util_random_gsl_rng = ompsize;
  util_random_gsl_rng = (gsl_rng**) malloc(n_util_random_gsl_rng*sizeof(gsl_rng*));

  for (ti=0; ti<n_util_random_gsl_rng; ti++) {
    util_random_gsl_rng[ti] = gsl_rng_alloc(rng_type);
    gsl_rng_set(util_random_gsl_rng[ti], seed + ti);
  }
}

static inline double _p_random_generate_uniform(int ompthread) {
  return gsl_rng_uniform(util_random_gsl_rng[ompthread]);
}

void random_finalize(void) {
  int ti;

  for (ti=0; ti<n_util_random_gsl_rng; ti++) {
    gsl_rng_free(util_random_gsl_rng[ti]);
  }
  free(util_random_gsl_rng);
  util_random_gsl_rng = NULL;
  n_util_random_gsl_rng = 0;
}

double random_uniform(int ompthread) {
  if (ompthread < n_util_random_gsl_rng) {
    return _p_random_generate_uniform(ompthread);
  }
  else {
    return NAN;
  }
}

double random_normal(int ompthread) {
  double u, v, s;

  do {
    u = 2.0*(random_uniform(ompthread) - 0.5); // uniform on [-1,1)
    v = 2.0*(random_uniform(ompthread) - 0.5); // uniform on [-1,1)
    s = u*u + v*v;
  } while (s > 1.0 || s <= 0.0);

  s = sqrt(-2.0*log(s)/s);

  return u*s;
}

void random_uniform_data(double *data, size_t length, double lo, double hi) {
  int ompthread = omp_get_thread_num();
  size_t i;

  for (i=0; i<length; i=i+1) {
    data[i] = (hi - lo)*random_uniform(ompthread) + lo;
  }
}

void random_normal_data(double *data, size_t length, double mean, double std) {
  int ompthread = omp_get_thread_num();
  size_t i;

  for (i=0; i<length; i=i+1) {
    data[i] = std*random_normal(ompthread) + mean;
  }
}

void random_transform_normal_to_lognormal(double *data, size_t length) {
  size_t i;

  for (i=0; i<length; i=i+1) {
    data[i] = exp(data[i]);
  }
}
