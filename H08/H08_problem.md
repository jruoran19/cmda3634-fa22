# Assigment 08 (30 points)


## Honor Code:
By submitting this assignment, you acknowledge that you have adhered to the
Virginia Tech Honor Code and attest to the following:
> I have neither given nor received unauthorized assistance on this assignment.
> The work I am presenting is ultimately my own.

### Using Google search
It is allowed to use Google to search for the meaning of compiler errors &
warnings, explanation for Git messages, or any sort of message that you are
being shown in the terminal.  Using Google skillfully to improve your
understanding of a subject is an important skill in programming.


## Submission
- The online submission of this homework is under your own and private Git
  project at: <https://code.vt.edu>
- The offline submission of this homework is in class or at the office 474
  McBryde. Slide the hard-copy under the door if it's closed (Note that the
  McBryde building has automatically locking doors that lock sometime in the
  evening.)
- You do not need to submit anything to Canvas.

### Submission deliverables
Submit these files via Git (before the due time of this assignment):
- **Q1** and **Q2**: `mc_int.c`
- **Q3**: 1x slurm output file
- **Q4**: 1x slurm output file

Print, staple, write your name, and turn-in on Wednesday in class (or before
Thursday at my office, 474 McBryde):
- **Q4**: Report


## Prepare
See the continuously updated
[cheat sheet for TinkerCliffs](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md).
The following steps are short descriptions and more details are found when
clicking the link, which gets you to one section of the TinkerCliffs cheat
sheet.

- Open the [terminal](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#terminal)
- Connect via [ssh](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#ssh)
- After ssh to TinkerCliffs was successful, continue by going to the directory
  of your Git repository (assuming the directory is called most likely `cmda3634`):
```bash
cd ~/cmda3634
```
- **New Git Workflow:** [Switch branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#switch-branches) to `main`:
```bash
git checkout main
```
- [Git-fetch and merge](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#receive-changes-from-remote-jrudi) the files from the remote named `jrudi`
- Locate the files of this assignment:
```bash
cd H08
```
- List the content with `ls`;  The files you will need to work with are
```bash
mc_int.c
Makefile
submission_debug.sh
submission_timing.sh
```
- [Load the correct modules](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#modules) to set up the right compiler.


## New things in this homework
There is slightly modified workflow for Git. Previously the steps were:
- `git fetch jrudi`
- `git merge jrudi/main`
- write code, compile, run
- `git add [FILES]`
- `git commit -m "message"`
- `git push`

**The new workflow for Git is:**
- `git checkout main` <- switch to main branch first
- `git fetch jrudi`
- `git merge jrudi/main`
- write code, compile, run
- `git branch H08` <- create a new branch named `H08`
- `git checkout H08` <- switch to the new branch named `H08`
- `git add [FILES]`
- `git commit -m "message"`
- `git push origin H08` <- specify to push the new branch


## Note
The reference solution has about 210 lines of code (including comments and spaces).

---

## Q1: Sequential MC integration (5 points)

This whole assigment will be about Monte Carlo integration.

### First, understand the background
Completing this homework becomes much easier if you understand the background
of Monte Carlo (MC) integration.  Here are two references to read about this topic:
1. [Wikipedia - Monte Carlo integration](https://en.wikipedia.org/wiki/Monte_Carlo_integration)
   is a concise read about the main formulas used for MC integration and variance estimation.
2. [Scratchapixel - Monte Carlo integration](https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/monte-carlo-methods-in-practice/monte-carlo-integration)
   is a extremely well-written explanation about MC integration. Highly recommended!

### Summary about Monte Carlo integration
#### Problem
**Given**:
- domain $\Omega$ in $R^m$ with volume ![V](https://wikimedia.org/api/rest_v1/media/math/render/svg/12a46c1bf758d1eb67ff118e49db8403246d7e0e)
- integrable function $f(x)$, $x\in\Omega$

**Want**:

![I = \int f(x) dx](https://wikimedia.org/api/rest_v1/media/math/render/svg/e55a9f3415b94c0ccb94ac2d818014d424f92f07)

#### Approximation of the integral with random numbers
Given $N$ random samples from a uniform probability distribution

![x_1, ..., x_N](https://wikimedia.org/api/rest_v1/media/math/render/svg/4158d757a3dc0f3d40298471e875175a4ec77e2a)

we can approximate the integral with the Monte Carlo method:

![Q_N](https://wikimedia.org/api/rest_v1/media/math/render/svg/80a984ae034987174d331e67cecc1fbebe71cc27)

#### How good is this approximation?
Because $Q_N$ is a random variable itself, we can write its mean as the limit

![\lim Q_N](https://wikimedia.org/api/rest_v1/media/math/render/svg/4b792c2d005fc31593c0d4aaffd24946b8add06f)

and its variance as

![Var(Q_N)](https://wikimedia.org/api/rest_v1/media/math/render/svg/38b76e478e93a5656e08a83340ec02ba5ef106d1)

The variance serves also as an error estimation for how good $Q_N$ approximates the integral $I$.

### Task

**Important**:
Familiarize yourself with the existing code in `mc_int.c`.  It has many
comments that are explaining what is happening.  And the functions have
descriptions of input arguments and outputs.

Goal: Implement the partially written function `mc_int_sequential`.

This function computes the results of the following formulas:
- Integral approximation:
```
Q_N = (1/N) * SUM_{samples x} f(x)/p(x)
```
- Squared-integral approximation:
```
S_N = (1/N) * SUM_{samples x} (f(x)/p(x))^2
```
- Variance approximation:
```
V_N = (S_N - (Q_N)^2) / (N - 1)
```
where
- `f(x)` is given as the function `integrand_fn(double x)`
- `p(x)` is given as the function `density_fn(double x)`
- `samples x` are provided by the function `random_fn(int ompthread)`
The division by `p(x)` in `Q_N` and `S_N` is a generalization, which is described in 
[Scratchapixel - Monte Carlo
integration](https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/monte-carlo-methods-in-practice/monte-carlo-integration),
Section "Generalization to Arbitrary PDF".

The partially implemented function is:
```c
void mc_int_sequential(double *int_approx, double *var_approx, size_t n_samples) {
  // declare variables
  size_t i;
  double x, y, p, avg1, avg2;

  // generate Monte Carlo samples
  avg1 = 0.0;
  avg2 = 0.0;
  for (i=0; i<n_samples; ++i) {
    x = random_fn(0);  // zero is the default thread id for single-threaded code
    y = integrand_fn(x);
    p = density_fn(x);
    avg1 += y / p;
    avg2 += (y*y) / (p*p);
  }

  // TODO finish computation of averages
  //avg1 = ...
  //avg2 = ...

  // TODO calculate approximate integral and variance
  //*int_approx = ...
  //*var_approx = ...
}
```
The variable `avg1` stores the result of the formula
```
Q_N = (1/N) * SUM_{samples x} f(x)/p(x)
```
and the variable `avg2` stores the result of the formula
```
S_N = (1/N) * SUM_{samples x} (f(x)/p(x))^2
```

The outputs of the function
```
void mc_int_sequential(double *int_approx, double *var_approx, size_t n_samples)
```
are
- `int_approx` <- `Q_N` and
- `var_approx` <- `V_N`

This means that inside the function, you have to set the values of the pointers 
`double *int_approx` and `double *var_approx`.

**Deliverable**: `mc_int.c`


## Q2: Parallel MC integration (10 points)

Goal: Implement function `mc_int_concurrent` using OpenMP

Here the sequential algorithm of **Q1** is parallelized.  We will use the
techniques learned in lectures 10-13 to parallelize with OpenMP.
The approach is to use dedicated arrays to store intermediate results of each
thread.  `TODO` comments are placed in the code to give a rough outline of the
implementation.

In the parallel regions, you **must** use OpenMP clauses to declare all
variables whether they are shared or private.

Hint: Before compiling, make sure that you loaded the modules with the provided script:
```bash
source setup_env.sh
```
Otherwise, the compilation will fail because of "missing" functions from an
external library.  Then compile (as usual) with `make`.  Use the job submission
script called `submission_debug.sh` to quickly test your implementation.  The
accuracy measured as error should be around (or below) $10^{-3}$.

**Deliverable**: `mc_int.c`


## Q3: Run test (5 points)

Before compiling, make sure that you loaded the modules with the provided script:
```bash
source setup_env.sh
```
This needs to be done **only once** after a fresh connection with ssh.

Compile (with the `make` command) and run the program.  For running the
program, **submit the job script** called `submission_debug.sh` using the
`sbatch` command.

Check the output in the output (`.out`) file from slurm.  Verify the
correctness of the code; example output:
```
========================================
  Test function(s), P = 1, N = 200100
----------------------------------------
- mc_int_sequential
test_mc_integral: MC integral = -2.421813e-03 (variance = 2.498836e-06), ref = 0 ; error = 2.422e-03
- mc_int_concurrent
test_mc_integral: MC integral = -1.115422e-03 (variance = 2.499913e-06), ref = 0 ; error = 1.115e-03
========================================
========================================
  Test function(s), P = 2, N = 200100
----------------------------------------
- mc_int_concurrent
test_mc_integral: MC integral = -1.843428e-03 (variance = 2.501422e-06), ref = 0 ; error = 1.843e-03
========================================
========================================
  Test function(s), P = 5, N = 200100
----------------------------------------
- mc_int_concurrent
test_mc_integral: MC integral = -5.696691e-04 (variance = 2.496422e-06), ref = 0 ; error = 5.697e-04
========================================
========================================
  Test function(s), P = 10, N = 200100
----------------------------------------
- mc_int_concurrent
test_mc_integral: MC integral = -3.406670e-04 (variance = 2.497955e-06), ref = 0 ; error = 3.407e-04
========================================
```

**Deliverable**: slurm output file


## Q4: Scalability study (5 points)

**Important**: First open `mc_int.c` find the line (almost at the top):
```c
#define ENABLE_DEBUG
```
and comment it out, so that it looks like this:
```c
//#define ENABLE_DEBUG
```

Change the compiler optimization settings in the `Makefile` to:
```
-O3
```

Compile or recompile (with the `make` command) and run the program.  For
running the program, **submit the job script** called `submission_timing.sh` using the
`sbatch` command.  Runtime is about 1 minute.

Write a report about the (strong) scalability that you can extract from the
wall clock times in the slurm output file.
As in lecture 14, create a table about the scalability that has four columns
with headers:
```
| #threads | wall clock time | speedup | efficiency |
```
Then create one row for each number of threads printed in the slurm output.

Under the table, write down three observations about the scalability. (Is it
optimal? Does it have issues?)

Hint: For me, the scalability did not look good between 1 and 16 threads. So,
don't be surprised to see mixed performance results.

**Deliverables**: one slurm output file; one report


## Q5: Submit results via Git into a new branch (5 points)

**New Git Workflow:** [Create a new branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#create-branches) in Git called `H08`:
```bash
git branch H08    # create a new branch named `H08`
git checkout H08  # switch to the new branch named `H08`
```

[Git add and commit](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#edit-add-commit-workflow)
the files that were previously listed as deliverables.

Double-check if your commit was successful, by looking at the output of 
```bash
git log -1  # for last commit
git log     # for all commits (scrollable with arrow keys; quit with `q`)
```

**New Git Workflow:** Upon successful commit, [push your changes](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#send-your-changes-eg-submit-homework)
```bash
git push origin H08  # tell Git to push the branch of the homework
```

After the `git push origin H08`, the changes will be uploaded to your Git
project at <https://code.vt.edu/>.  Double-check that these files actually made
it there: Go to the webpage <https://code.vt.edu/> and locate the files you
were supposed to push.  Click on a file to see that your changes were actually
transferred.
