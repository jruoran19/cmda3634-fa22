#!/bin/bash

# Help: Submit this file as a job request with
#   sbatch submission.sh

########################################

# The following is for the job scheduler, slurm.
# It is read at the time of the `sbatch ...` command.

#SBATCH -N 1             # total number of nodes
#SBATCH -n 1             # total number of tasks (MPI)
#SBATCH -c 128           # number of cores per task
#SBATCH --exclusive
#SBATCH -t 00:20:00      # max runtime
#SBATCH -p normal_q      # queue
#SBATCH -A cmda3634_rjh  # allocation

########################################

if [[ `hostname` =~ tinkercliffs.* ]]; then
  echo "You are not allowed to run on the login node, just on the compute nodes."
  echo "Usage: sbatch submission.sh"
  exit 1
fi

# The following are the commands that are executed on compute nodes, once the
# submitted job is done waiting in the queue and actually runs.

# change to the directory from which the job was submitted
cd $SLURM_SUBMIT_DIR

# set up modules
source setup_env.sh

# print list of modules
echo "========================================"
echo "Modules:"
echo ""
echo "$(module -t list 2>&1 | sort)"
echo "========================================"

# set up OpenMP
export OMP_PROC_BIND=true

# start timer
time_begin=`date +%s`

# run the program
export OMP_NUM_THREADS=1;   ./mc_int
export OMP_NUM_THREADS=2;   ./mc_int
export OMP_NUM_THREADS=4;   ./mc_int
export OMP_NUM_THREADS=8;   ./mc_int
export OMP_NUM_THREADS=16;  ./mc_int
export OMP_NUM_THREADS=32;  ./mc_int
export OMP_NUM_THREADS=64;  ./mc_int
export OMP_NUM_THREADS=128; ./mc_int

# end and print timer
time_end=`date +%s`
echo "Runtime [sec]: $(($time_end - $time_begin))"
