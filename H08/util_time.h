#include <stddef.h>

void time_fill_bins(int*, float*, int, float*, size_t,
                    void(*)(int*, float*, int, float*, size_t));

void time_pi_approx(size_t, double(*)(size_t));

void time_mc_integral(size_t, void(*)(double*,double*,size_t));
