#include <util_test.h>
#include <util_random.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void test_find_bins(float *bin_ranges, int bin_count, float *data, size_t length,
                    void(*fn_ref)(int*, float*, int, float*, size_t),
                    void(*fn_chk)(int*, float*, int, float*, size_t)) {
  int *bins_ref, *bins_chk;
  int j;
  long unsigned int sum = 0;

  /* allocate */
  bins_ref = (int*) malloc(bin_count*sizeof(int));
  bins_chk = (int*) malloc(bin_count*sizeof(int));

  /* fill bins with ref and chk functions*/
  fn_ref(bins_ref, bin_ranges, bin_count, data, length);
  fn_chk(bins_chk, bin_ranges, bin_count, data, length);

  /* print errors */
  for (j=0; j<bin_count; j++) {
    printf("%s: %4d | [%+08.4f,%+08.4f) | difference=%d (ref=%d vs. chk=%d)\n",
           __func__, j, bin_ranges[j], bin_ranges[j+1],
           bins_ref[j] - bins_chk[j], bins_ref[j], bins_chk[j]);
    sum += (long unsigned int) abs(bins_ref[j] - bins_chk[j]);
  }
  printf("Sum of of all abs(diffrence): %lu\n", sum);
  fflush(stdout); // flush buffered print outputs

  /* deallocate */
  free(bins_ref);
  free(bins_chk);
}

void test_pi_approx(size_t n_samples, double(*fn)(size_t)) {
  double pi_ref = 3.141592653589793238;
  double pi_chk;

  pi_chk = fn(n_samples);
  printf("%s: pi approx = %.16f, ref = %.16f ; error abs = %.3e, rel = %.3e\n",
         __func__, pi_chk, pi_ref,
         fabs(pi_chk - pi_ref),
         fabs(pi_chk - pi_ref)/fabs(pi_ref));
  fflush(stdout); // flush buffered print outputs
}

void test_mc_integral(size_t n_samples, void(*fn)(double*,double*,size_t)) {
  double int_ref = 0.0;
  double int_chk = NAN, var_chk = NAN;

  fn(&int_chk, &var_chk, n_samples);
  printf("%s: MC integral = %.6e (variance = %.6e), ref = %g ; error = %.3e\n",
         __func__, int_chk, var_chk, int_ref, fabs(int_chk - int_ref));
  fflush(stdout); // flush buffered print outputs
}
