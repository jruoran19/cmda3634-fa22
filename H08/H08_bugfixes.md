# H08: Bugs and how to fix them

- Bug: `make` command gives error
```
util_random.c:4:10: fatal error: gsl/gsl_rng.h: No such file or directory
    4 | #include <gsl/gsl_rng.h>
```
Fix: Load all necessary modules using the command:
```
source setup_env.sh
```
This will load the module of the [GSL library](https://www.gnu.org/software/gsl/) 
that is used by the program.
Loading modules needs to be done only once after establishing a fresh connection via ssh.
