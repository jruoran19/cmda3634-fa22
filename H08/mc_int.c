#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <util_random.h>
#include <util_test.h>
#include <util_time.h>

/* uncomment the line below to run in debugging mode (for testing code)
 * comment the line below to run in production mode (for timing code) */
//#define ENABLE_DEBUG

/* set the problem sizes */
#define PROBLEM_SIZE_DEBUG 200100
#define PROBLEM_SIZE_TIMING 30200100

/* set up the problem depending on debugging mode */
#if defined(ENABLE_DEBUG)
#define PROBLEM_SIZE PROBLEM_SIZE_DEBUG
#else
#define PROBLEM_SIZE PROBLEM_SIZE_TIMING
#endif

/**
 * Evaluates the integrand for 0<=x<=1.
 */
double integrand_fn(double x) {
#if defined(ENABLE_DEBUG)
  /* evaluate a sin function, where we know what it integrates to */
  const double pi = 3.141592653589793238;
  return sin(2*pi*x);
#else
  /* evaluate a Gaussian mixture function */
  const double c0 = 0.33;
  const double c1 = 0.67;
  const double d0 = 100.0;
  const double d1 = 100.0;
  return exp(-d0*(x - c0)*(x - c0)) + 
         exp(-d1*(x - c1)*(x - c1));
#endif
}

/**
 * Evaluates the probability density for 0<=x<=1 of uniform distribution.
 */
double density_fn(double x) {
  return 1.0;
}

/**
 * Generates sample from a uniform distribution.
 */
double random_fn(int ompthread) {
  return random_uniform(ompthread);
}

/**
 * Performs sequential Monte Carlo integration in 1D of the function
 * `integrand_fn`.  The approximation of the integrand is computed along with
 * an approximated variance, which is interpreted as the (square of the)
 * integration error.
 *
 * Input:
 * n_samples  Number of Monte Carlo samples
 *
 * Output:
 * int_approx  Approximate value of the integration
 * int_approx  Approximate value variance (i.e., error^2 of integration)
 */
void mc_int_sequential(double *int_approx, double *var_approx, size_t n_samples) {
  // declare variables
  size_t i;
  double x, y, p, avg1, avg2;

  // generate Monte Carlo samples
  avg1 = 0.0;
  avg2 = 0.0;
  for (i=0; i<n_samples; ++i) {
    x = random_fn(0);  // zero is the default thread id for single-threaded code
    y = integrand_fn(x);
    p = density_fn(x);
    avg1 += y / p;
    avg2 += (y*y) / (p*p);
  }

  // finish computation of averages
  avg1 = avg1 / n_samples;
  avg2 = avg2 / n_samples;

  // calculate approximate integral and variance
  *int_approx = avg1;
  *var_approx = (avg2 - avg1*avg1) / (n_samples-1);
}

/**
 * Performs OpenMP-parallel Monte Carlo integration in 1D of the function
 * `integrand_fn`.  The approximation of the integrand is computed along with
 * an approximated variance, which is interpreted as the (square of the)
 * integration error.
 *
 * Input:
 * n_samples  Number of Monte Carlo samples
 *
 * Output:
 * int_approx  Approximate value of the integration
 * int_approx  Approximate value variance (i.e., error^2 of integration)
 */
void mc_int_concurrent(double *int_approx, double *var_approx, size_t n_samples) {
  // declare variables
  int ompsize;
  size_t i;
  double x, y, p, *avg1, *avg2;

  #pragma omp parallel shared(ompsize)
  {
    ompsize = omp_get_num_threads();
  }
  // create workspace/storage of threads
  avg1 = (double*)malloc(sizeof(double)*ompsize);
  avg2 = (double*)malloc(sizeof(double)*ompsize);

  #pragma omp parallel shared(avg1, avg2, n_samples, ompsize) private(i, x, y, p) // tell what variables are shared or private
  {
    // generate Monte Carlo samples in parallel
    i = omp_get_thread_num();
    size_t chunk = n_samples/(size_t)ompsize;
    if (i == ompsize-1) {
      chunk += n_samples % (size_t)ompsize;
    }
    avg1[i] = 0;
    avg2[i] = 0;
    for (size_t j = 0; j < chunk; j++)
    {
      x = random_fn(i);
      y = integrand_fn(x);
      p = density_fn(x);
      avg1[i] += y / p;
      avg2[i] += (y*y) / (p*p);
    }
  }

  // combine results from all threads
  double sum_avg1 = 0, sum_avg2 = 0;
  for (size_t i = 0; i < ompsize; i++)
  {
    sum_avg1 += avg1[i];
    sum_avg2 += avg2[i];
  }

  // destroy workspace/storage of threads
  free(avg1);
  free(avg2);

  // finish computation of averages
  double Q_N = sum_avg1 / n_samples;
  double S_N = sum_avg2 / n_samples;

  // calculate approximate integral and variance
  *int_approx = Q_N;
  *var_approx = (S_N - (Q_N*Q_N)) / (n_samples-1);
}

int main(int argc, char **argv) {
  const size_t N_SAMPLES = PROBLEM_SIZE;
  int ompsize;

  #pragma omp parallel shared(ompsize)
  { // FORK: begin parallel
    ompsize = omp_get_num_threads();
  } // JOIN: end parallel

  /* initialize random number generator */
  random_initialize(ompsize);

  printf("========================================\n");

#if defined(ENABLE_DEBUG)
  printf("  Test function(s), P = %d, N = %lu\n", ompsize, (long unsigned int) N_SAMPLES);
  printf("----------------------------------------\n");
  fflush(stdout); // flush buffered print outputs

  if (1 == ompsize) {
    printf("- mc_int_sequential\n");
    test_mc_integral(N_SAMPLES, mc_int_sequential);
  }

  printf("- mc_int_concurrent\n");
  test_mc_integral(N_SAMPLES, mc_int_concurrent);
#else
  printf("  Time function(s), P = %d, N = %lu\n", ompsize, (long unsigned int) N_SAMPLES);
  printf("----------------------------------------\n");
  fflush(stdout); // flush buffered print outputs

  if (1 == ompsize) {
    printf("- mc_int_sequential\n");
    time_mc_integral(N_SAMPLES, mc_int_sequential);
  }

  printf("- mc_int_concurrent\n");
  time_mc_integral(N_SAMPLES, mc_int_concurrent);
#endif

  printf("========================================\n");

  /* finalize random number generator */
  random_finalize();

  return 0;
}

