#include <util_time.h>
#include <util_random.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <omp.h>

/* set the number of repeats for timing */
#define TIMING_REPEAT 4

void time_fill_bins(int *bins, float *bin_ranges, int bin_count, float *data, size_t length,
                    void(*fn)(int*, float*, int, float*, size_t)) {
  int k;
  clock_t tic, toc;
  double elapsed = 0.0;

  for (k=0; k<TIMING_REPEAT+1; k++) {
    /* call function and time */
    tic = clock();
    fn(bins, bin_ranges, bin_count, data, length);
    toc = clock();
    /* calculate elapsed time */
    if (0 < k) {
      elapsed += (toc-tic)/(double)CLOCKS_PER_SEC;
    }
  }

  printf("%s: elapsed time ~ %.3f s, time per call ~ %.3e s\n", 
         __func__, elapsed, elapsed/TIMING_REPEAT);
  fflush(stdout); // flush buffered print output
}

void time_pi_approx(size_t n_samples, double(*fn)(size_t)) {
  int k;
  double tic, toc;
  double elapsed = 0.0;

  for (k=0; k<TIMING_REPEAT+1; k++) {
    /* call function and time */
    tic = omp_get_wtime();
    fn(n_samples);
    toc = omp_get_wtime();
    /* calculate elapsed time */
    if (0 < k) {
      elapsed += toc - tic;
    }
  }

  printf("%s: elapsed time ~ %.3f s, time per call ~ %.3e s\n",
         __func__, elapsed, elapsed/TIMING_REPEAT);
  fflush(stdout); // flush buffered print output
}

void time_mc_integral(size_t n_samples, void(*fn)(double*,double*,size_t)) {
  double int_chk = NAN, var_chk = NAN;
  int k;
  double tic, toc;
  double elapsed = 0.0;

  for (k=0; k<TIMING_REPEAT+1; k++) {
    /* call function and time */
    tic = omp_get_wtime();
    fn(&int_chk, &var_chk, n_samples);
    toc = omp_get_wtime();
    /* calculate elapsed time */
    if (0 < k) {
      elapsed += toc - tic;
    }
  }

  printf("%s: result: int=%g, var=%g ; elapsed time ~ %.3f s, time per call ~ %.3e s\n",
         __func__, int_chk, var_chk, elapsed, elapsed/TIMING_REPEAT);
  fflush(stdout); // flush buffered print output
}
