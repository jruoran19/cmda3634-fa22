# H11 help

## How to choose arguments in `<<< >>>` brackets
When implementing a kernel call, how many threads and blocks should be used
in between the `<<< >>>` brackets?

Note that the `<<< >>>` brackets accept numbers (e.g. `1`, `12`, `123`) as
well as variables that are integers.

In the `main` function, there are two variables, one is:
```
const int n_threads = N_THREADS_PER_BLOCK;
```
which needs to be used as the number of threads per block in `<<< >>>`.
`n_threads` is declared as a constant, because there is no need for you to
change it.

The other variable is:
```
int n_blocks = -1;
```
which needs to be used as the number of blocks in `<<< >>>`.
This variable **must** be set as part of the homework solution.
To set `n_blocks` correctly, consider how many times you would need to call
the kernel; this should amount to the problem size `N`.
Therefore, the product `n_blocks*n_threads` needs to be at least `N` in order
for all the work to be completed. One needs to set `n_blocks` as the solution
of this equation:
```
n_blocks*n_threads = N
```
So, we get
```
n_blocks = N/n_threads
```
where the right-hand side is likely a floating point number.
But `n_blocks` needs to be integer valued, so what we want in the code is
```
n_blocks = ceil(N/n_threads)
```
that is, we want to round up. Then there needs to be at least as many blocks as
there is work for each thread-block.

Hint: In Lecture 21 we have seen a way of computing the number of blocks
without using ceil (and exploiting integer arithmetic).

## How to choose arguments `offset` and `n`
The role of the variables `offset` and `n` is that in the blur filter,
```
y[i]= 0.25*(x[i-1] + 2.0*x[i] + x[i+1])
```
the index `i` is in the range
```
offset <= i < n
```

To reason what the correct values of `offset` and `n` are, we can ask:
1. what is the smallest possible value of `offset` (while `offset >= 0`, is
   bounded from below)
2. what is the largest possible value of `n` (while `n < N`, is bounded from
   above by the problem size)

For 1. the answer is found through noticing `x[i-1]`, because `i` has to be at
least `1` for this to run correctly.
While for 2. the answer is found through noticing `x[i+1]`, because `i` can be
at most `N-2` for this to run correctly.

Combining our reasoning, we found that
```
1 <= i < N-1
```
and this is the solution of the question above.

## How to design the double-blur kernel
The general idea is to do
```
y[i] = 0.25*(x[i-1] + 2*x[i] + x[i+1]) ... that's one blur kernel call computing entries in y from x
z[i] = 0.25*(y[i-1] + 2*y[i] + y[i+1]) ... that's a second the blur kernel call computing entries in z from y
```

The goal with the double-blur kernel is to replace the two kernel calls by one. So, inside the double-blur kernel:
- 1st, do one blur by computing entries in buffer from x
- 2nd, do second blur by computing entries in z from buffer

To implement that double-blur kernel, you can start with the code from the buffered (single blur) kernel. Then replace setting the entries of buffer from 
```
buffer[li+1] = x[gi]
```
to computing "one blur"
```
buffer[li+1] = 0.25*(x[gi-1] + 2.0*x[gi] + x[gi+1])
```
then the buffer stores one computation of the kernel. And afterward z is computed from the entries of the buffer, which computes the second blurring.

Notice the if statement `li == 0`, how would you replace
```
buffer[li] = x[gi-1]   ?
```

Notice the if statement `li == (blockDim.x-1) || gi == (n-1)`, how would you replace
```
buffer[li+2] = x[gi+1]   ?
```
