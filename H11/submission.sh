#!/bin/bash

# Help: Submit this file as a job request with
#   sbatch submission.sh

########################################

# The following is for the job scheduler, slurm.
# It is read at the time of the `sbatch ...` command.

#SBATCH -N 1             # total number of nodes
#SBATCH -n 1             # total number of tasks (MPI)
#SBATCH --gres=gpu:1
#SBATCH -t 00:01:00      # max runtime
#SBATCH -p p100_dev_q    # queue
#SBATCH -A cmda3634_rjh  # allocation
#SBATCH -o slurm-%j.out  # output file name

########################################

if [[ `hostname` =~ tinkercliffs.* ]]; then
  echo "You are not allowed to run on the login node, just on the compute nodes."
  echo "Usage: sbatch submission.sh"
  exit 1
fi

# The following are the commands that are executed on compute nodes, once the
# submitted job is done waiting in the queue and actually runs.

# change to the directory from which the job was submitted
cd $SLURM_SUBMIT_DIR

# set up modules
source setup_env.sh

# print list of modules
echo "========================================"
echo "Modules:"
echo ""
echo "$(module -t list 2>&1 | sort)"
echo "========================================"

# start timer
time_begin=`date +%s`

# run the program
nvprof ./blur_1d

# end and print timer
time_end=`date +%s`
echo "Runtime [sec]: $(($time_end - $time_begin))"
