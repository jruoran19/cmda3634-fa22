#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <math.h>

/* set the problem size */
#define PROBLEM_SIZE 100200300

/* set the number of CUDA threads per block */
#define N_THREADS_PER_BLOCK 256

/**
 * CUDA kernel function: Applies intensity preserving blur filter in 1D.
 */
__global__ void k_blur_1d(float *x, float *y, int offset, int n) {
  // implement the kernel function as described in the problem statement
  int thread_id = blockIdx.x*blockDim.x+threadIdx.x;
  int i = offset + thread_id;
  if (i < n) {
    y[i] = 0.25*(x[i-1]+2.0*x[i]+x[i+1]);
  }
}

/**
 * CUDA kernel function: Applies intensity preserving blur filter in 1D.
 * Uses a buffer in block-shared memory for data reuse.
 */
__global__ void k_blur_1d_buffered(float *x, float *y, int offset, int n) {
  // implement the kernel function as described in the problem statement
  extern __shared__ float buffer[];
  int thread_id = blockIdx.x*blockDim.x+threadIdx.x;
  int gi = offset + thread_id;
  int li = threadIdx.x;
  if (gi < n) {
    if (li == 0) {
      buffer[li] = x[gi-1];
    }
    buffer[li+1] = x[gi];
    if (li == (blockDim.x - 1) || gi == (n-1)) {
      buffer[li+2] = x[gi+1];
    }
  }
  __syncthreads();
  if (gi < n) {
    y[gi] = 0.25*(buffer[li]+2.0*buffer[li+1]+buffer[li+2]);
  }
}

/**
 * CUDA kernel function: Applies intensity preserving blur filter in 1D twice.
 * Uses a buffer in block-shared memory for data reuse.
 */
__global__ void k_double_blur_1d(float *x, float *z, int offset, int n) {
  // implement the kernel function as described in the problem statement
  extern __shared__ float buffer[];
  int thread_id = blockIdx.x*blockDim.x+threadIdx.x;
  int gi = offset + thread_id;
  int li = threadIdx.x;
  if (gi < n) {
    if (li == 0) {
      buffer[li] = x[gi-1];
    }
    buffer[li+1] = x[gi];
    if (li == (blockDim.x - 1) || gi == (n-1)) {
      buffer[li+2] = x[gi+1];
    }
  }
  __syncthreads();
  if (gi < n) {
    z[gi] = 0.25*(buffer[li]+2.0*buffer[li+1]+buffer[li+2]);
  }
  __syncthreads();
  gi++;
  n--;
  if (gi < n) {
    if (li == 0) {
      buffer[li] = z[gi-1];
    }
    buffer[li+1] = z[gi];
    if (li == (blockDim.x - 1) || gi == (n-1)) {
      buffer[li+2] = z[gi+1];
    }
  }
  __syncthreads();
  if (gi < n) {
    z[gi] = 0.25*(buffer[li]+2.0*buffer[li+1]+buffer[li+2]);
  }
}

/**
 * Helper functions.
 */
void CUDA_CHKERR(cudaError_t);
void initialize_arrays(float*, float*, float*, float*, float*, float*, size_t);
float check_host_array(float*, size_t, size_t, float);

/**
 * Main function.
 */
int main(int argc, char **argv) {
  const size_t N = PROBLEM_SIZE;
  const int n_threads = N_THREADS_PER_BLOCK;
  int n_blocks = -1;
  int shared_size = -1;
  float *h_x=NULL, *h_y=NULL, *h_z=NULL; // arrays for host memory (access with CPU)
  float *d_x=NULL, *d_y=NULL, *d_z=NULL; // arrays for device memory (access with GPU)
  float max_error;

  /* allocate arrays in host memory (CPU) */
  h_x = (float*) malloc(N*sizeof(float));
  h_y = (float*) malloc(N*sizeof(float));
  h_z = (float*) malloc(N*sizeof(float));

  /* allocate arrays in device memory (GPU) */
  // allocate `d_x` and `d_y` and `d_z`, having `N` entries, using `cudaMalloc()`
  cudaMalloc(&d_x, sizeof(float)*N);
  cudaMalloc(&d_y, sizeof(float)*N);
  cudaMalloc(&d_z, sizeof(float)*N);

  /* set number of blocks */
  // set variable `n_blocks` depending on problem size `N` and `n_threads`
  // such that there are enough blocks to for the entire problem size
  n_blocks = (N + (n_threads-1)) / n_threads;

  printf("========================================\n");
  printf("N = %lu, n_blocks=%d, n_threads=%d\n", (long unsigned int) N, n_blocks, n_threads);
  fflush(stdout); // flush buffered print outputs

  if (!h_x || !h_y || !h_z || !d_x || !d_y || !d_z) {  // if previous TODOs incomplete
    fprintf(stderr, "Allocations were not completed!\n");
    return 1;
  }
  if (n_blocks < 0) {  // if previous TODOs incomplete
    fprintf(stderr, "Setting number of blocks was not completed!\n");
    return 1;
  }

  /*
   * Part 1
   */

  /* initialize x and y arrays (CPU) */
  initialize_arrays(h_x, h_y, h_z, d_x, d_y, d_z, N);

  /* transfer data from host to device memory (CPU->GPU) */
  // transfer array `*_x` from host memory to device memory with `cudaMemcpy()`
  cudaMemcpy(d_x, h_x, sizeof(float)*N, cudaMemcpyHostToDevice);

  /* run the kernel function `k_blur_1d` (GPU) */
  // call kernel function with the previously set variables for number of
  // blocks and threads per block
  k_blur_1d<<<n_blocks, n_threads>>>(d_x, d_y, 1, N-1);

  CUDA_CHKERR(cudaGetLastError());
  /* wait for GPU threads to complete */
  CUDA_CHKERR( cudaDeviceSynchronize() );

  /* transfer data from device to host memory (GPU->CPU) */
  // transfer array `*_y` from device memory to host memory with `cudaMemcpy()`
  cudaMemcpy(h_y, d_y, sizeof(float)*N, cudaMemcpyDeviceToHost);

  /* check errors (CPU) */
  max_error = check_host_array(h_y, 1, N-1, 1.25);
  printf("----------------------------------------\n");
  printf("k_blur_1d\n");
  printf("  Max error = %e\n", max_error);

  /*
   * Part 2
   */

  /* initialize x and y arrays (CPU) */
  initialize_arrays(h_x, h_y, h_z, d_x, d_y, d_z, N);

  /* transfer data from host to device memory (CPU->GPU) */
  // transfer array `*_x` from host memory to device memory with `cudaMemcpy()`
  cudaMemcpy(d_x, h_x, sizeof(float)*N, cudaMemcpyHostToDevice);

  /* set the size of block-shared memory */
  // figure out how much memory (in Bytes) your kernel needs, and set `shared_size`
  shared_size = (n_threads+1)*sizeof(float);

  /* run the kernel function `k_blur_1d_buffered` (GPU) */
  // call kernel function with the previously set variables for number of
  // blocks and threads per block; 
  // and with the previously set size of block-shared memory
  k_blur_1d_buffered<<<n_blocks, n_threads, shared_size>>>(d_x, d_y, 1, N-1);

  CUDA_CHKERR(cudaGetLastError());
  /* wait for GPU threads to complete */
  CUDA_CHKERR( cudaDeviceSynchronize() );

  /* transfer data from device to host memory (GPU->CPU) */
  // transfer array `*_y` from device memory to host memory with `cudaMemcpy()`
  cudaMemcpy(h_y, d_y, sizeof(float)*N, cudaMemcpyDeviceToHost);

  /* check errors (CPU) */
  max_error = check_host_array(h_y, 1, N-1, 1.25);
  printf("----------------------------------------\n");
  printf("k_blur_1d_buffered\n");
  printf("  Max error = %e\n", max_error);

  /*
   * Part 3
   */

  /* initialize x and y arrays (CPU) */
  initialize_arrays(h_x, h_y, h_z, d_x, d_y, d_z, N);

  /* transfer data from host to device memory (CPU->GPU) */
  // transfer array `*_x` from host memory to device memory with `cudaMemcpy()`
  cudaMemcpy(d_x, h_x, sizeof(float)*N, cudaMemcpyHostToDevice);

  /* run the kernel function `k_blur_1d` twice (GPU) */
  // call kernel function with the previously set variables for number of
  // blocks and threads per block; and then call it again (don't forget to
  // adjust arguments for offset and n)
  k_blur_1d<<<n_blocks, n_threads>>>(d_x, d_y, 1, N-1);
  k_blur_1d<<<n_blocks, n_threads>>>(d_y, d_z, 2, N-2);

  CUDA_CHKERR(cudaGetLastError());
  /* wait for GPU threads to complete */
  CUDA_CHKERR( cudaDeviceSynchronize() );

  /* transfer data from device to host memory (GPU->CPU) */
  // transfer array `*_z` from device memory to host memory with `cudaMemcpy()`
  cudaMemcpy(h_z, d_z, sizeof(float)*N, cudaMemcpyDeviceToHost);

  /* check errors (CPU) */
  max_error = check_host_array(h_z, 2, N-2, 1.25);
  printf("----------------------------------------\n");
  printf("k_blur_1d, k_blur_1d\n");
  printf("  Max error = %e\n", max_error);

  /*
   * Part 4
   */

  /* initialize x and y arrays (CPU) */
  initialize_arrays(h_x, h_y, h_z, d_x, d_y, d_z, N);

  /* transfer data from host to device memory (CPU->GPU) */
  // transfer array `*_x` from host memory to device memory with `cudaMemcpy()`
  cudaMemcpy(d_x, h_x, sizeof(float)*N, cudaMemcpyHostToDevice);

  /* set the size of block-shared memory */
  // figure out how much memory (in Bytes) your kernel needs, and set `shared_size`
  shared_size = (n_threads+1)*sizeof(float);

  /* run the kernel function `k_double_blur_1d` (GPU) */
  // call kernel function with the previously set variables for number of
  // blocks and threads per block; 
  // and with the previously set size of block-shared memory
  k_double_blur_1d<<<n_blocks, n_threads, shared_size>>>(d_x, d_z, 1, N-1);

  CUDA_CHKERR(cudaGetLastError());
  /* wait for GPU threads to complete */
  CUDA_CHKERR( cudaDeviceSynchronize() );

  /* transfer data from device to host memory (GPU->CPU) */
  // transfer array `*_z` from device memory to host memory with `cudaMemcpy()`
  cudaMemcpy(h_z, d_z, sizeof(float)*N, cudaMemcpyDeviceToHost);

  /* check errors (CPU) */
  max_error = check_host_array(h_z, 2, N-2, 1.25);
  printf("----------------------------------------\n");
  printf("k_double_blur_1d\n");
  printf("  Max error = %e\n", max_error);

  printf("========================================\n");
  fflush(stdout); // flush buffered print outputs

  /* deallocate arrays in host memory (CPU) */
  free(h_x);
  free(h_y);
  free(h_z);

  /* deallocate arrays in device memory (GPU) */
  // deallocate all pointers to device memory
  cudaFree(d_x);
  cudaFree(d_y);
  cudaFree(d_z);

  return 0;
}

/* DO NOT CHANGE THE FUNCTIONS BELOW */

void CUDA_CHKERR(cudaError_t err) {
  if (err != cudaSuccess) {
    printf("CUDA error: %s\n", cudaGetErrorString(err));
    fflush(stdout); // flush buffered print outputs
    exit(1);
  }
}

__global__ void k_init_d_arrays(float *x, float *y, float *z, int n) {
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < n) {
    x[i] = NAN;
    y[i] = NAN;
    z[i] = NAN;
  }
}

void initialize_arrays(float *h_x, float *h_y, float *h_z,
                       float *d_x, float *d_y, float *d_z, size_t n) {
  size_t i;

  for (i=0; i<n; i++) {
    h_x[i] = (i%2 ? 1.3 : 1.2);
    h_y[i] = NAN;
    h_z[i] = NAN;
  }

  k_init_d_arrays<<< (n + 31)/32, 32 >>>(d_x, d_y, d_z, n);
}

float check_host_array(float *arr, size_t offset, size_t n, float true_val) {
  size_t i;
  float max_error = fabs(arr[offset] - true_val);

  for (i=offset; i<n; i++) {
    max_error = fmax(max_error, fabs(arr[i] - true_val));
  }
  return max_error;
}
