# Assigment 11 (36 points)


## Honor Code:
By submitting this assignment, you acknowledge that you have adhered to the
Virginia Tech Honor Code and attest to the following:
> I have neither given nor received unauthorized assistance on this assignment.
> The work I am presenting is ultimately my own.

### Using Google search
It is allowed to use Google to search for the meaning of compiler errors &
warnings, explanation for Git messages, or any sort of message that you are
being shown in the terminal.  Using Google skillfully to improve your
understanding of a subject is an important skill in programming.


## Submission
- The online submission of this homework is under your own and private Git
  project at: <https://code.vt.edu>
- The offline submission of this homework is in class or at the office 474
  McBryde. Slide the hard-copy under the door if it's closed (Note that the
  McBryde building has automatically locking doors that lock sometime in the
  evening.)
- You do not need to submit anything to Canvas.

### Submission deliverables
Submit these files via Git (before the due time of this assignment):
- **Q1** - **Q4**: `blur_1d.cu`
- **Q5**: One slurm output file

No printed report this time.


## Note for this homework

**Don't** ssh to TinkerCliffs.

Instead ssh to Infer (`infer1.arc.vt.edu`).

For calls to `cudaXYZ` functions (e.g., `cudaMalloc`, `cudaFree`, `cudaMemcpy`)
check for CUDA errors with `CUDA_CHKERR` as in the following example:
```
CUDA_CHKERR( cudaMalloc(...) );
```
This will notify you of errors when running the code. It's very helpful for debugging.


## Prepare
See the continuously updated
[cheat sheet for ARC clusters](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md).
The following steps are short descriptions and more details are found when
clicking the link, which gets you to one section of the cheat sheet.

- Open the [terminal](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#terminal)
- Connect via ssh to ARC's Infer cluster:
  ```bash
  ssh [PID]@infer1.arc.vt.edu
  ```
- After ssh to Infer was successful, continue by going to the directory
  of your Git repository (assuming the directory is called most likely `cmda3634`):
  ```bash
  cd ~/cmda3634
  ```
- **New Git Workflow:** [Switch branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#switch-branches) to `main`:
  ```bash
  git checkout main
  ```
- [Git-fetch and merge](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#receive-changes-from-remote-jrudi) the files from the remote named `jrudi`
- Locate the files of this assignment:
  ```bash
  cd H11
  ```
- List the content with `ls`;  The files you will need to work with are
  ```bash
  blur_1d.cu
  Makefile
  setup_env.sh
  submission.sh
  ```
- [Load the correct modules](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#modules) to set up the right compiler.

---

## Q1: Implement blur kernel (6 points)

**Deliverable**: Code in `blur_1d.cu`

### Q1.A (2 points)
Implement the CUDA kernel function `k_blur_1d` that applies the blur filter
as discussed in lecture 21.  The blur filter computes
```
y[i] = 1/4 * (x[i-1] + 2*x[i] + x[i+1])
```
for each `i` in `[1,N-1)`.

The kernel function has the following pseudo code:
```
FUNCTION: k_blur_1d
INPUT:  x[n], offset, n
OUTPUT: y[n]

i = offset + global_thread_id
IF i < n
  y[i]= 0.25*(x[i-1] + 2.0*x[i] + x[i+1])
```
where 
- `global_thread_id` is the thread index computed from `blockIdx.x`, `blockDim.x`, and `threadIdx.x`
- `offset` needs to be an integer such that `i-1` does not become negative
- `n` needs to be such that `i+1` does not become larger-or-equal `n`

### Q1.B (4 points)
In the `main` function, implement the memory allocation and deallocation of device memory.
- allocate float-arrays in device memory `d_x` and `d_y` and `d_z`, having `N`
  entries
- deallocate device memory `d_x` and `d_y` and `d_z`

Implement the calculation of the number of blocks, `n_blocks`.
Effectively one needs to compute `ceil(N/n_threads)`, where `N` is the problem
size and `n_threads` is the number of threads per block.  Is there a way of
computing `n_block` using only integer arithmetic?

**Now, consider only Part 1 in the main function**:
Implement the memory transfers between host and device in the `main` function
in indicated in the `TODO`s.

Then implement the kernel call of `k_blur_1d`, where `d_x` is the input data
and `d_y` is the output of the kernel. Pay attention when choosing the
arguments `offset` and `n` of the kernel function (see above).

Try running the code at this point, using make and sbatch. Check if the error
of the function you implemented is zero.


## Q2: Implement buffered blur kernel (6 points)

**Deliverable**: Code in `blur_1d.cu`

### Q2.A (4 points)
Implement the CUDA kernel function `k_blur_1d_buffered` that applies the blur filter
with buffering of input data.

The kernel function has the following pseudo code, but it is **missing** the
declaration of the buffer variable and the `__syncthreads()` function.
```
FUNCTION: k_blur_1d_buffered
INPUT:  x[n], offset, n
OUTPUT: y[n]

gi = offset + global_thread_id
li = threadIdx.x
IF gi < n
  IF li == 0
    buffer[li] = x[gi-1]
  buffer[li+1] = x[gi]
  IF li == (blockDim.x - 1) or gi == (n-1)
    buffer[li+2] = x[gi+1]
IF gi < n
  y[gi]= 0.25*(buffer[li] + 2.0*buffer[li+1] + buffer[li+2])
```
where 
- `global_thread_id`, `offset` and `n`  is as before
- `local_thread_id` is just `threadIdx.x`

First, figure out where to call `__syncthreads()` and how to declare the array `buffer`.
Then implement your solution.

### Q2.B (2 points)
**Now, consider only Part 2 in the main function**:
Implement the calculation of the size of the block-shared memory, `shared_size`.
Implement the memory transfers between host and device in the `main` function
in indicated in the `TODO`s.

Then implement the kernel call of `k_blur_1d_buffered`, where `d_x` is the input data
and `d_y` is the output of the kernel. Pay attention when choosing the
arguments `offset` and `n` of the kernel function (see above).

Try running the code at this point, using make and sbatch. Check if the error
of the function you implemented is zero.


## Q3: Implement two calls to blur kernel (4 points)

**Deliverable**: Code in `blur_1d.cu`

### Q3.A (4 points)
**Now, consider only Part 3 in the main function**:
Implement the memory transfers between host and device in the `main` function
in indicated in the `TODO`s.

Then implement the kernel call of `k_blur_1d`, where `d_x` is the input data
and `d_y` is the output of the kernel. 
Implement a second kernel call of `k_blur_1d`, where `d_y` is the input data
and `d_z` is the output of the kernel.
Pay attention when choosing the arguments `offset` and `n` of the kernel
function (see above).
They are going to be different for the two calls.

Try running the code at this point, using make and sbatch. Check if the error
of the function you implemented is zero.


## Q4: Implement buffered double blur kernel (10 points)

**Deliverable**: Code in `blur_1d.cu`

### Q4.A (8 points)
Implement the CUDA kernel function `k_double_blur_1d` that applies the blur filter
**twice** with buffering of intermediate data.

Instead of using three arrays as in **Q3** (`d_x` as input, `d_y` as
intermediate storage, and `d_z` as output), use only `d_x` and `d_z` and no
intermediate storage.

Design a pseudo-code for yourself, and then implement the kernel.
(The pseudo-code is just for you.)

### Q4.B (2 points)
**Now, consider only Part 4 in the main function**:
Implement the calculation of the size of the block-shared memory, `shared_size`.
Implement the memory transfers between host and device in the `main` function
in indicated in the `TODO`s.

Then implement the kernel call of `k_double_blur_1d`. Pay attention when choosing the
arguments `offset` and `n` of the kernel function (see above).

Try running the code at this point, using make and sbatch. Check if the error
of the function you implemented is zero.


## Q5: Compile and run (5 points)

### Info about compiling and running
Before compiling, make sure that you loaded the modules with the provided script:
```bash
source setup_env.sh
```

Compile the code with:
```bash
make
```

Run the program with:
```bash
sbatch submission.sh
```

### Tasks
After all the implementations from above are satisfactory, have **just one**
slurm output file that you will submit for grading.

**Deliverable**: slurm output file `slurm-XYZ.out`


## Q6: Submit results via Git into a new branch (5 points)

[Create a new branch](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#create-branches) in Git called `H11`:
```bash
git branch H11    # create a new branch named `H11`
git checkout H11  # switch to the new branch named `H11`
```

[Git add and commit](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#edit-add-commit-workflow)
the files that were previously listed as deliverables.

Double-check if your commit was successful, by looking at the output of 
```bash
git log -1  # for last commit
git log     # for all commits (scrollable with arrow keys; quit with `q`)
```

Upon successful commit, [push your changes](https://code.vt.edu/jrudi/cmda3634-fa22/-/blob/main/L09/tinkercliffs-cheatsheet.md#send-your-changes-eg-submit-homework)
```bash
git push origin H11  # tell Git to push the branch of the homework
```

After the `git push origin H11`, the changes will be uploaded to your Git
project at <https://code.vt.edu/>.  Double-check that these files actually made
it there:
1. Go to the webpage <https://code.vt.edu/>
2. Open a drop-down menu that has `main` selected (top-center of webpage), and
   select the branch `H11`
3. Locate the files you were supposed to push.  Click on a file to see that
   your changes were actually transferred.
