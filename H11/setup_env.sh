#!/bin/bash

# Help: Run this script as: source setup_env.sh

# set modules to system's default
module reset

# load compilers
#module load gompi
#module load foss/2020a
module load fosscuda/2020b
#module load intel/2019b
#module load intelcuda/2020b
